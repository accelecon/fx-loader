//--------------------------------------------------------------------------
//  cm.c
//--------------------------------------------------------------------------

#include "main.h"

ccptr CmVersion = NULL;

void CmOpen( void) {
  bit32 BaudRate = 115200;
  bit32 BaudRateDivisor = MasterClock       / (BaudRate * 16)      ;
  bit32 BaudRateError = ((MasterClock * 10) / (BaudRate * 16)) % 10;
  if (BaudRateError > 4) {
    BaudRateDivisor += 1;
  }
  MemPut32( AT91C_PIOC_PDR, AT91C_PC16_UTXD1 | AT91C_PC17_URXD1);
  MemPut32( AT91C_UART1_IDR, ~0);
  MemPut32( AT91C_UART1_CR, AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS);
  MemPut32( AT91C_UART1_BRGR, BaudRateDivisor);
  MemPut32( AT91C_UART1_MR, AT91C_US_PAR_NONE);
  MemPut32( AT91C_UART1_CR, AT91C_US_RXEN | AT91C_US_TXEN);
  SysType = e_OptiCell; // tentative, will be overridden shortly
}

void CmPutChar( bit8 Data) {
  if (SysType == e_OptiCell) { // needs tentative SysType of e_OptiCell from CmOpen() to get version
    while ((MemGet32( AT91C_UART1_CSR) & AT91C_US_TXRDY) == 0) ;
    MemPut32( AT91C_UART1_THR, Data);
} }

void CmPutBuffer( cbptr Buffer, bit32 Length) {
  while (Length--) {
    CmPutChar( *Buffer++);
} }

bit8 CmGetReady( void) {
  return (MemGet32( AT91C_UART1_CSR) & AT91C_US_RXRDY) ? 1 : 0;
}

bool CmGetByte( bptr Temp) {
  if (CmGetReady()) {
    *Temp = MemGet32( AT91C_UART1_RHR) & 0xff;
    return true;
  }
  return false;
}

// Receive buffer and control variables.

static bit8 CmRecvBuffer[32];

static bit8 CmBytes;
static bit8 CmIdle ;
static bit8 CmQuery;

// Receive any incoming bytes from the charge monitor subsystem and
// append them to CmRecvBuffer.  If we have bytes in the buffer and we
// don't see any new ones for 500 us (10 cycles of 50 us) we return
// true.

static bool CmGetString( void) {
  bit8 Data;
  DelayRaw( DelayRaw_50us);
  if (CmGetByte( &Data)) {
    if (CmBytes < 60) {
      CmRecvBuffer[CmBytes++] = Data;
    }
    CmIdle = 0;
  } else {
    if (CmIdle < 10) {
      CmIdle++;
    } else {
      if (CmBytes) {
        while (CmBytes && !(CmRecvBuffer[CmBytes-1] & 0xe0)) {
          CmRecvBuffer[--CmBytes] = 0;
        }
        if (CmBytes) {
          CmRecvBuffer[CmBytes] = 0;
          return true;
  } } } }
  return false;
}

// Data from the uart1 serial interface comes in at 115.2Kbps, or one
// byte every 87 us.  If we check for new data every 50 us we won't
// miss any bytes, and if we don't see any new bytes for 500 us then
// we know that the current burst has completed and we are safe to
// output it to the console.

// After 100 ms we send a version query, and if we get a response we
// can be confident that we are an OptiCell and not a NetBridge.

void CmTest( void) {
  bit32 Time, NoExit = 0, Sec10 = 10000000/50; // ten seconds in 50 us units
  TtyPutStrLn( "Monitoring CM for 10 seconds (* to toggle timeout)...");
  CmBytes = CmIdle = CmQuery = 0;
  for (Time = 0; Time < Sec10; Time++) {
    if (NoExit) {
      Time = 0;
    }
    if (CmGetString()) {
      TtyPutStrLn( (ccptr) CmRecvBuffer);
      CmBytes = CmIdle = CmQuery = 0;
    }
    if (TtyGetReady()) {
      char c = TtyGetChar();
      if (c == '*') {
        NoExit = NoExit ? 0 : 1;
      } else {
        CmPutChar( c);
  } } }
  TtyPutLn();
}

// Send a 'V' version query twice followed by a 100 ms listen window
// during which we wait for a recognizable response.  If we see a
// valid firmware response, return a pointer to our receive buffer,
// otherwise return NULL.

bool CmGetVersion( void) {
  bit32 Loop, Time, Ms100 = 100000/50; // 100 ms in 50 us units
  CmOpen();
  for (Loop = 0; Loop < 2; Loop++) {
    CmBytes = CmIdle = CmQuery = 0;
    CmPutBuffer( (bptr) "\rF\r", 3);
    for (Time = 0; Time < Ms100; Time++) {
      if (CmGetString()) {
        if (Compare( CmRecvBuffer, (bptr) "F=", 2, NULL) == 0) {
          CmVersion = (ccptr) CmRecvBuffer;
          return true;
  } } } }
  return false;
}

static void CmPutBit32( bit32 Value) {
  bit8 Digit = Value % 10;
       Value = Value / 10;
  if (Value) {
    CmPutBit32( Value);
  }
  CmPutChar( Digit + '0');
}

void CmBeep( bit32 Hz, bit32 Ticks) {
  CmPutBuffer( (bptr) "B=", 2);
  CmPutBit32( Hz);
  CmPutChar( ',');
  CmPutBit32( Ticks);
  CmPutChar( 0x0d);
}

//--------------------------------------------------------------------------
//  end
//--------------------------------------------------------------------------
