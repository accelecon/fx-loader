//-------------------------------------------------------------------------------------------
//  spi.h
//-------------------------------------------------------------------------------------------

typedef struct {
	bit8  Command[8]; // align on 32-bit
  bit32 ChipSelect; // align on 32-bit
	bit32 DataSize  ; // align on 32-bit
	bptr  Data      ; // align on 32-bit
} SpiData;

#define	SPI_ERROR 0x01 // must match FLASH_ERROR from flash.h
#define SPI_OK    0x02 // must match FLASH_OK    from flash.h

typedef bit8 SPI_Status   ;

extern void       SPI_SpiInit     ( bit32 ChipSelect, SpiData * Spi);
extern SPI_Status SPI_SpiReadWrite( bit32 CmdSize   , SpiData * Spi);

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

