//-------------------------------------------------------------------------------------------
//  flash.h
//-------------------------------------------------------------------------------------------

typedef bit8 FLASH_Status;

#define FLASH_BUSY             0x00
#define FLASH_ERROR            0x01 // must match SPI_ERROR from spi.h
#define FLASH_OK               0x02 // must match SPI_OK    from spi.h
#define FLASH_MEMORY_OVERFLOW  0x03
#define FLASH_BAD_COMMAND      0x04
#define FLASH_BAD_ADDRESS      0x05
#define FLASH_BAD_PAGESIZE     0x06

extern FLASH_Status JF_Init ( bit32 PageSize);
extern void         JF_Print( void);

extern FLASH_Status JF_Erase ( bit32 FlashAddress, bit32 FlashBytes);
extern FLASH_Status JF_Clear ( bit32 FlashAddress, bit32 FlashBytes);
extern FLASH_Status JF_Write ( bit32 FlashAddress, bit32 FlashBytes);
extern FLASH_Status JF_Verify( bit32 FlashAddress, bit32 FlashBytes);
extern FLASH_Status JF_Read  ( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer);

extern const cptr JedecFlash;

// Macronix SPI Flash - MX25L25635E - 256 Mb, 32 MB.  Although this part has
// uniform 4 KB pages for program and erase, we will utilize 64 KB blocks.

#define JF_BLOCKSIZE     KB(64) // block size in flash
#define JF_BLOCKS_TOTAL    512  // blocks in MX25L25632E flash chip

#define JF_BLOCKS_LOADER      3 //   192 KB
#define JF_BLOCKS_UBOOT       4 //   256 KB
#define JF_BLOCKS_UBENV       1 //    64 KB
#define JF_BLOCKS_KERNEL0    32 //  2048 KB
#define JF_BLOCKS_KERNEL1    32 //  2048 KB
#define JF_BLOCKS_FILESYS0  204 // 13056 KB
#define JF_BLOCKS_FILESYS1  204 // 13056 KB
#define JF_BLOCKS_CONFIG     16 //  1024 KB
#define JF_BLOCKS_STORE      16 //  1024 KB

#define JF_BLOCK_LOADER     0                                       //            =   0
#define JF_BLOCK_UBOOT     (JF_BLOCK_LOADER   + JF_BLOCKS_LOADER  ) //    0 +   3 =   3
#define JF_BLOCK_UBENV     (JF_BLOCK_UBOOT    + JF_BLOCKS_UBOOT   ) //    3 +   4 =   7
#define JF_BLOCK_KERNEL0   (JF_BLOCK_UBENV    + JF_BLOCKS_UBENV   ) //    7 +   1 =   8
#define JF_BLOCK_KERNEL1   (JF_BLOCK_KERNEL0  + JF_BLOCKS_KERNEL0 ) //    8 +  32 =  40
#define JF_BLOCK_FILESYS0  (JF_BLOCK_KERNEL1  + JF_BLOCKS_KERNEL1 ) //   40 +  32 =  72
#define JF_BLOCK_FILESYS1  (JF_BLOCK_FILESYS0 + JF_BLOCKS_FILESYS0) //   72 + 204 = 276
#define JF_BLOCK_CONFIG    (JF_BLOCK_FILESYS1 + JF_BLOCKS_FILESYS1) //  276 + 204 = 480
#define JF_BLOCK_STORE     (JF_BLOCK_CONFIG   + JF_BLOCKS_CONFIG  ) //  480 +  16 = 496
                                                                    //  496 +  16 = 512

#define JF_ADDR_LOADER     (JF_BLOCK_LOADER   * JF_BLOCKSIZE) //     0K  0000.0000
#define JF_ADDR_UBOOT      (JF_BLOCK_UBOOT    * JF_BLOCKSIZE) //   192K  0003.0000
#define JF_ADDR_UBENV      (JF_BLOCK_UBENV    * JF_BLOCKSIZE) //   448K  0007.0000
#define JF_ADDR_KERNEL0    (JF_BLOCK_KERNEL0  * JF_BLOCKSIZE) //   512K  0008.0000
#define JF_ADDR_KERNEL1    (JF_BLOCK_KERNEL1  * JF_BLOCKSIZE) //  2560K  0028.0000
#define JF_ADDR_FILESYS0   (JF_BLOCK_FILESYS0 * JF_BLOCKSIZE) //  4608K  0048.0000
#define JF_ADDR_FILESYS1   (JF_BLOCK_FILESYS1 * JF_BLOCKSIZE) // 17664K  0114.0000
#define JF_ADDR_CONFIG     (JF_BLOCK_CONFIG   * JF_BLOCKSIZE) // 30720K  01e0.0000
#define JF_ADDR_STORE      (JF_BLOCK_STORE    * JF_BLOCKSIZE) // 31744K  01f0.0000
                                                              // 32768K  0200.0000

// Arbitrary enumeration values.

#define MX25L12835E 0x68  // arbitrary - 2**N for 256 Mbit part + 0x60 for Macronix
#define MX25L25635E 0x69  // arbitrary - 2**N for 256 Mbit part + 0x60 for Macronix

// Memory-mapped base address of JedecFlash.

#define JF_ADDR_BASE  0xd0000000

// Memory-mapped boot addresses of the two uImage partitions.

#define JF_ADDR_BOOT0  "D0080000"
#define JF_ADDR_BOOT1  "D0280000"

//-------------------------------------------------------------------------------------------
// end
//-------------------------------------------------------------------------------------------

