//-------------------------------------------------------------------------------------------
//  main.h
//-------------------------------------------------------------------------------------------

typedef       unsigned char     bit8;
typedef const unsigned char *  cbptr;
typedef       unsigned char *   bptr;
typedef                char *   cptr;
typedef const          char *  ccptr;

typedef unsigned     short int bit16;
typedef unsigned      long int bit32;
typedef unsigned long long int bit64;

typedef   signed      long int int32;

typedef enum {
  false = 0,
  true  = 1
} bool;

typedef enum {
  e_Generic   = 0,
  e_OptiCell  = 1,
  e_NetBridge = 2
} e_SysType;

#include "acfx100.h"    // board-specific constants
#include "macros.h"

#include "start.h"
#include "util.h"
#include "led.h"
#include "time.h"

#include "gpio.h"
#include "clock.h"
#include "sdram.h"
#include "images.h"
#include "ub-env.h"

#include "cm.h"
#include "lcd.h"
#include "spi.h"
#include "flash.h"

#include <string.h>

extern bit8 iCacheEnabled;
extern bit8 LoaderProtect;

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

