//-------------------------------------------------------------------------------------------
//  acfx100.h - constants and macros for the NetBridge-FX and OptiCell-FX boards
//-------------------------------------------------------------------------------------------

#include "at91sam9x25.h"  // Atmel chip-specific constants

//-------------------------------------------------------------------------------------------
//  additional chip-specific constants
//-------------------------------------------------------------------------------------------

#define BOARD_CPU_TYPE        "Atmel AT91SAM9X25"

#define BOARD_SRAM_BASE       0x00300000
#define BOARD_SRAM_SIZE       KB(32)      // 32 KB

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for SPI0 --> SPI
//-------------------------------------------------------------------------------------------

#define BOARD_SPI_MOSI        (AT91C_PA12_SPI0_MOSI)
#define BOARD_SPI_MISO        (AT91C_PA11_SPI0_MISO)
#define BOARD_SPI_SPCK        (AT91C_PA13_SPI0_SPCK)
#define BOARD_SPI_NPCS0       (AT91C_PA14_SPI0_NPCS0)
#define BOARD_SPI_NPCS1       (AT91C_PA7_SPI0_NPCS1)

#define BOARD_SPI_CR          (AT91C_SPI0_CR)
#define BOARD_SPI_MR          (AT91C_SPI0_MR)
#define BOARD_SPI_SR          (AT91C_SPI0_SR)
#define BOARD_SPI_CSR         (AT91C_SPI0_CSR)
#define BOARD_SPI_TDR         (AT91C_SPI0_TDR)
#define BOARD_SPI_RDR         (AT91C_SPI0_RDR)

#define BOARD_ID_SPI          (AT91C_ID_SPI0)

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for ETH0 --> ETH
//-------------------------------------------------------------------------------------------

#define BOARD_ID_EMAC         (AT91C_ID_EMAC0)

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for UHPHS --> USB
//-------------------------------------------------------------------------------------------

#define BOARD_ID_USB          (AT91C_ID_UHPHS)

//-------------------------------------------------------------------------------------------
//  board-to-gpio mappings
//-------------------------------------------------------------------------------------------

// ethernet control and status

#define BOARD_PORTA_ETH_ENABLE  BIT( 0)   // output, active high
#define BOARD_PORTA_ETH_ACTIVE  BIT( 1)   // input, active low
#define BOARD_PORTA_ETH_LINK    BIT( 2)   // input, active low

// tact switch labeled 'default' and green led

#define BOARD_PORTD_SW1         BIT( 0)   // input, requires pullup, active low
#define BOARD_PORTD_LED_GRN     BIT( 1)   // output, active low

// external and internal usb port power control

#define BOARD_PORTD_USBX_DIS_OC BIT( 4)   // output, pulled low, low to enable usb power

#define BOARD_PORTD_USBIA_DIS   BIT( 5)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBIB_DIS   BIT( 6)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBIC_DIS   BIT( 7)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBID_DIS   BIT( 8)   // output, pulled low, low to enable usb power

// serial interface to lcd module (115200n81)

#define BOARD_PORTC_LCD_TXD     BIT( 8)   // output
#define BOARD_PORTC_LCD_RXD     BIT( 9)   // input

// serial interface to cm module (115200n81)

#define BOARD_PORTC_CM_TXD      BIT(16)   // output
#define BOARD_PORTC_CM_RXD      BIT(17)   // input

// full-color leds for usb and 3g status

#define BOARD_PORTC_LED_USB_R   BIT( 3)   // output, pulled low, active low
#define BOARD_PORTC_LED_USB_G   BIT( 4)   // output, pulled low, active low
#define BOARD_PORTC_LED_USB_B   BIT( 5)   // output, active low

#define BOARD_PORTC_LED_3G_R    BIT( 6)   // output, active low
#define BOARD_PORTC_LED_3G_G    BIT( 7)   // output, active low
#define BOARD_PORTC_LED_3G_B    BIT( 8)   // output, active low

// simple green leds for signal strength bar

#define BOARD_PORTC_LED_SIG_1   BIT( 9)   // output, active high
#define BOARD_PORTC_LED_SIG_2   BIT(10)   // output, active high
#define BOARD_PORTC_LED_SIG_3   BIT(11)   // output, active high
#define BOARD_PORTC_LED_SIG_4   BIT(12)   // output, active high
#define BOARD_PORTC_LED_SIG_5   BIT(13)   // output, active high

// internal and external usb port power control and monitoring

#define BOARD_PORTD_USBI_DIS    BIT( 4)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBX_DIS_NB BIT( 5)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBI_FLG    BIT( 8)   // input, pulled high, overcurrent detect
#define BOARD_PORTD_USBX_FLG    BIT( 9)   // input, pulled high, overcurrent detect

//-------------------------------------------------------------------------------------------
//  general board architecture
//
//    SDRAM -  32 MB - NCS1  - Micron MT48LC16M16A2
//             64 MB - NCS1  - ISSI IS42S16320B-7TLI
//    FLASH -  32 MB - NPCS1 - Macronix MX25L25635E JEDEC SPI Flash
//-------------------------------------------------------------------------------------------

#define BOARD_CLOCK_XTAL      12000000                //  12.000 MHz

#define BOARD_SDRAM_BASE      0x20000000              // NCS1
#define BOARD_SDRAM_UBOOT     0x20700000              // BOARD_SDRAM_BASE+MB(7)
#define BOARD_SDRAM_LINUX_OFF 0x00008000              // 32 KB
#define BOARD_SDRAM_LINUX     BOARD_SDRAM_BASE+BOARD_SDRAM_LINUX_OFF

#define BOARD_FLASH_CS_JEDEC  BOARD_SPI_NPCS1         // Macronix MX25L25635E

// http://www.arm.linux.org.uk/developer/machines/download.php

#define MACH_ACFX100OC        4124
#define MACH_ACFX100NB        4125

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

