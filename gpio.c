//-------------------------------------------------------------------------------------------
//  gpio.c
//-------------------------------------------------------------------------------------------

#include "main.h"

e_SysType SysType = e_Generic;

//-------------------------------------------------------------------------------------------
//  configure gpio pins as inputs with optional pullups
//-------------------------------------------------------------------------------------------

void GpioConfigInput( bit32 Port, bit32 Mask, bit8 Pullup) {
  MemPut32( Port +                              AT91C_PIOA_IDR   , Mask);
  MemPut32( Port + (Pullup ? AT91C_PIOA_PPUER : AT91C_PIOA_PPUDR), Mask);
  MemPut32( Port +                              AT91C_PIOA_ODR   , Mask);
  MemPut32( Port +                              AT91C_PIOA_IFDR  , Mask);
  MemPut32( Port +                              AT91C_PIOA_PER   , Mask);
}

//-------------------------------------------------------------------------------------------
//  configure gpio pins as outputs and set initial state
//-------------------------------------------------------------------------------------------

void GpioConfigOutput( bit32 Port, bit32 Mask, bit8 InitialValue) {
  MemPut32( Port +                                   AT91C_PIOA_MDDR , Mask);
  MemPut32( Port +                                   AT91C_PIOA_IDR  , Mask);
  MemPut32( Port +                                   AT91C_PIOA_PPUDR, Mask);
  MemPut32( Port + (InitialValue ? AT91C_PIOA_SODR : AT91C_PIOA_CODR), Mask);
  MemPut32( Port +                                   AT91C_PIOA_OER  , Mask);
  MemPut32( Port +                                   AT91C_PIOA_PER  , Mask);
}

//-------------------------------------------------------------------------------------------
//  switch gpio pins to peripheral A function
//-------------------------------------------------------------------------------------------

static void GpioConfigPeripheralA( bit32 Port, bit32 Mask) {
  MemPut32( Port + AT91C_PIOA_IDR  , Mask);
  MemPut32( Port + AT91C_PIOA_PPUDR, Mask);
  MemPut32( Port + AT91C_PIOA_SP1  , MemGet32( AT91C_PIOA_SP1) & ~Mask); // 0
  MemPut32( Port + AT91C_PIOA_SP2  , MemGet32( AT91C_PIOA_SP2) & ~Mask); // 0
  MemPut32( Port + AT91C_PIOA_PDR  , Mask);
}

//-------------------------------------------------------------------------------------------
//  switch gpio pins to peripheral C function
//-------------------------------------------------------------------------------------------

static void GpioConfigPeripheralC( bit32 Port, bit32 Mask) {
  MemPut32( Port + AT91C_PIOA_IDR  , Mask);
  MemPut32( Port + AT91C_PIOA_PPUDR, Mask);
  MemPut32( Port + AT91C_PIOA_SP1  , MemGet32( AT91C_PIOA_SP1) & ~Mask); // 0
  MemPut32( Port + AT91C_PIOA_SP2  , MemGet32( AT91C_PIOA_SP2) |  Mask); // 1
  MemPut32( Port + AT91C_PIOA_PDR  , Mask);
}

//-------------------------------------------------------------------------------------------
//  enable all gpio ports and configure input/output pins
//-------------------------------------------------------------------------------------------

void GpioConfig( e_SysType ConfigSysType) {
  switch (SysType = ConfigSysType) {
    case e_Generic:
      MemPut32( AT91C_PMC_PCER, 1 << AT91C_ID_PIOA_B); // enable pio a/b controller
      MemPut32( AT91C_PMC_PCER, 1 << AT91C_ID_PIOC_D); // enable pio c/d controller
      MemPut32( AT91C_PMC_PCER, 1 << BOARD_ID_SPI   ); // enable spi
      MemPut32( AT91C_PMC_PCER, 1 << AT91C_ID_UART0 ); // enable uart0
      MemPut32( AT91C_PMC_PCER, 1 << AT91C_ID_UART1 ); // enable uart1
   // MemPut32( AT91C_PMC_PCER, 1 << BOARD_ID_EMAC  ); // enable ethernet
      GpioConfigOutput( GPIO_PORTA, BOARD_PORTA_ETH_ENABLE      
                                  | BOARD_SPI_NPCS0
                                  | BOARD_SPI_NPCS1, 1); // initially on
      GpioConfigInput ( GPIO_PORTA, BOARD_PORTA_ETH_ACTIVE
                                  | BOARD_PORTA_ETH_LINK, 0); // no pullup needed
      GpioConfigInput ( GPIO_PORTD, BOARD_PORTD_SW1, 1); // requires pullup
      GpioConfigPeripheralA( GPIO_PORTA, BOARD_SPI_MOSI | BOARD_SPI_MISO | BOARD_SPI_SPCK); // SPI
      GpioConfigPeripheralC( GPIO_PORTC, BOARD_PORTC_LCD_TXD
                                       | BOARD_PORTC_LCD_RXD
                                       | BOARD_PORTC_CM_TXD
                                       | BOARD_PORTC_CM_RXD); // additional serial i/o
      break;
    case e_OptiCell:
      GpioConfigOutput( GPIO_PORTD, BOARD_PORTD_LED_GRN, 1); // initially off
      GpioConfigOutput( GPIO_PORTD, BOARD_PORTD_USBX_DIS_OC
                                  | BOARD_PORTD_USBIA_DIS
                                  | BOARD_PORTD_USBIB_DIS
                                  | BOARD_PORTD_USBIC_DIS
                                  | BOARD_PORTD_USBID_DIS, 0); // initially powered
      break;
    case e_NetBridge:
      GpioConfigOutput( GPIO_PORTC, BOARD_PORTC_LED_USB_R 
                                  | BOARD_PORTC_LED_USB_G
                                  | BOARD_PORTC_LED_USB_B
                                  | BOARD_PORTC_LED_3G_R
                                  | BOARD_PORTC_LED_3G_G
                                  | BOARD_PORTC_LED_3G_B, 1); // initially on
      GpioConfigOutput( GPIO_PORTC, BOARD_PORTC_LED_SIG_1
                                  | BOARD_PORTC_LED_SIG_2
                                  | BOARD_PORTC_LED_SIG_3
                                  | BOARD_PORTC_LED_SIG_4
                                  | BOARD_PORTC_LED_SIG_5, 0); // initially off
      GpioConfigOutput( GPIO_PORTD, BOARD_PORTD_USBI_DIS
                                  | BOARD_PORTD_USBX_DIS_NB, 0); // initially powered
      break;
} }

//-------------------------------------------------------------------------------------------
//  output new values to gpio output pins
//-------------------------------------------------------------------------------------------

void GpioPut( bit32 Port, bit32 Mask, bit8 Value) {
  MemPut32( Port + (Value ? AT91C_PIOA_SODR : AT91C_PIOA_CODR), Mask);
}

//-------------------------------------------------------------------------------------------
//  input current values from gpio pins
//-------------------------------------------------------------------------------------------

bit32 GpioGet( bit32 Port, bit32 Mask) {
  return MemGet32( Port + AT91C_PIOA_PDSR) & Mask;
}

//-------------------------------------------------------------------------------------------
//  pulse a gpio output for 500 ms (active=1/0 for active-high/low)
//-------------------------------------------------------------------------------------------

static void GpioPulse( bit32 Port, bit32 Mask, bit8 Active) {
  GpioPut( Port, Mask, Active ? 1 : 0); DelayMs( 500);
  GpioPut( Port, Mask, Active ? 0 : 1);
}

//-------------------------------------------------------------------------------------------
//  test gpio outputs by pulsing leds and usb power
//-------------------------------------------------------------------------------------------

void GpioTest( void) {
  bit8 Phase = 0;
  switch (SysType) {
    case e_Generic:
      break;
    case e_NetBridge:
      while (Phase < 21) {
        switch (Phase++) {
          case  0: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_USB_R                                                , 0); break;
          case  1: GpioPulse( GPIO_PORTC,                         BOARD_PORTC_LED_USB_G                        , 0); break;
          case  2: GpioPulse( GPIO_PORTC,                                                 BOARD_PORTC_LED_USB_B, 0); break;
          case  3: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_USB_R | BOARD_PORTC_LED_USB_G                        , 0); break;
          case  4: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_USB_R |                         BOARD_PORTC_LED_USB_B, 0); break;
          case  5: GpioPulse( GPIO_PORTC,                         BOARD_PORTC_LED_USB_G | BOARD_PORTC_LED_USB_B, 0); break;
          case  6: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_USB_R | BOARD_PORTC_LED_USB_G | BOARD_PORTC_LED_USB_B, 0); break;

          case  7: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_3G_R                                              , 0); break;
          case  8: GpioPulse( GPIO_PORTC,                        BOARD_PORTC_LED_3G_G                       , 0); break;
          case  9: GpioPulse( GPIO_PORTC,                                               BOARD_PORTC_LED_3G_B, 0); break;
          case 10: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_3G_R | BOARD_PORTC_LED_3G_G                       , 0); break;
          case 11: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_3G_R |                        BOARD_PORTC_LED_3G_B, 0); break;
          case 12: GpioPulse( GPIO_PORTC,                        BOARD_PORTC_LED_3G_G | BOARD_PORTC_LED_3G_B, 0); break;
          case 13: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_3G_R | BOARD_PORTC_LED_3G_G | BOARD_PORTC_LED_3G_B, 0); break;

          case 14: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_SIG_1, 1); break;
          case 15: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_SIG_2, 1); break;
          case 16: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_SIG_3, 1); break;
          case 17: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_SIG_4, 1); break;
          case 18: GpioPulse( GPIO_PORTC, BOARD_PORTC_LED_SIG_5, 1); break;

          case 19: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBI_DIS   , 0); break;
          case 20: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBX_DIS_NB, 0); break;
      } }
      break;
    case e_OptiCell:
      while (Phase < 6) {
        switch (Phase++) {
          case 0: GpioPulse( GPIO_PORTD, BOARD_PORTD_LED_GRN    , 0); break;
          case 1: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBX_DIS_OC, 0); break;
          case 2: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBIA_DIS  , 0); break;
          case 3: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBIB_DIS  , 0); break;
          case 4: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBIC_DIS  , 0); break;
          case 5: GpioPulse( GPIO_PORTD, BOARD_PORTD_USBID_DIS  , 0); break;
      } }
      break;
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

