//-------------------------------------------------------------------------------------------
//  flashjedec.c - Macronix SPI Flash Support
//-------------------------------------------------------------------------------------------

#include "main.h"

const cptr JedecFlash = "Macronix";

// Flash interface data.

static struct {
  SpiData Spi       ;
  bit8    TypeCode  ;
  bit8    Status    ;
  bit32   PageSize  ;
  bit32   BlockSize ;
  bit32   SectorSize;
  bit32   TotalBytes;
} JF;

//-------------------------------------------------------------------------------------------
// Macronix SPI Flash interface layer.
//-------------------------------------------------------------------------------------------

#define JEDEC_00_MACRONIX  0xC2

#define MX25L12835E_01  0x20
#define MX25L12835E_02  0x18

#define MX25L25635E_01  0x20
#define MX25L25635E_02  0x19

#define OPCODE_PP         0x02  // 256-byte page program
#define OPCODE_RDSR       0x05  // read status register
#define OPCODE_WREN       0x06  // write enable
#define OPCODE_READ_FAST  0x0b  // read fast
#define OPCODE_P4E        0x20  // 4 KB parameter block erase
#define OPCODE_RDID       0x9f  // JEDEC read id
#define OPCODE_EN4B       0xb7  // enter 4-byte address mode
#define OPCODE_EX4B       0xe9  // exit 4-byte address mode

#define RDSR_WIP          0x01  // write in progress (bit in status register)

// Keep track of addressing mode.  Warning!  Although this is a volatile setting in the
// FLASH chip, it survives system reset due to the error on RESET hookup (pin 1 vs. pin 3).
// Therefor if the SAM9X25 is reset while the FLASH is in 32-bit addressing mode, the SAM9X25
// machine loader will not be able to load the boot loader from FLASH!  Power cycling the
// SAM9X25 subsystem will, however, clear the FLASH chip back to 24-bit mode.

static bit8 CurrentLength = 3;

static void JF_AddressLength( bit8 DesiredLength) {
  if (CurrentLength != DesiredLength) {
    if (DesiredLength == 3) JF.Spi.Command[0] = OPCODE_EX4B;
    if (DesiredLength == 4) JF.Spi.Command[0] = OPCODE_EN4B;
    JF.Spi.DataSize = 0;
    if (SPI_SpiReadWrite( 1, &JF.Spi) == SPI_OK) {
      CurrentLength = DesiredLength;
} } }

// Wait for any write or erase operation to complete.

static FLASH_Status JF_WaitReady( void) {
//bit32 Timeout = MasterClock / 1200;
  bit32 Timeout = MasterClock;
  JF.Status = 0;
  JF.Spi.DataSize = 0;
  do {
    JF.Spi.Command[0] = OPCODE_RDSR;
    JF.Spi.Command[1] = 0;
    if (SPI_SpiReadWrite( 2, &JF.Spi) == SPI_OK) {
      JF.Status = JF.Spi.Command[1];
    }
    Timeout--;
  } while ((JF.Status & RDSR_WIP) && Timeout);
  JF_AddressLength( 3);
  return (JF.Status & RDSR_WIP) ? FLASH_ERROR : FLASH_OK;
}

// Send an SPI command which carries an address.  Upshift to 32-bit addressing automatically the
// first time we need to.  Also note that Fast Reads need a dummy byte (8 clock cycles).

static FLASH_Status JF_SpiReadWriteBuffer( bit8 OpCode, bit32 FlashAddress, bptr FlashBuffer, bit32 FlashBytes) {
  if (FlashAddress & 0x0f000000) {
    JF_AddressLength( 4);
  }
  bit8 CmdLength = 0;
  JF.Spi.Command[ CmdLength++] = OpCode;
  if (CurrentLength == 4) {
    JF.Spi.Command[ CmdLength++] = (bit8) ((FlashAddress & 0x0f000000) >> 24);
  }
  JF.Spi.Command[ CmdLength++] = (bit8) ((FlashAddress & 0x00ff0000) >> 16);
  JF.Spi.Command[ CmdLength++] = (bit8) ((FlashAddress & 0x0000ff00) >>  8);
  JF.Spi.Command[ CmdLength++] = (bit8) ( FlashAddress & 0x000000ff)       ;
  if (OpCode == OPCODE_READ_FAST) {
    JF.Spi.Command[ CmdLength++] = 0; // 8 dummy cycles (1 dummy byte)
  }
  JF.Spi.Data     = FlashBuffer;
  JF.Spi.DataSize = FlashBytes ;
  return (SPI_SpiReadWrite( CmdLength, &JF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Set the write-enable bit in the control register.  This must be done
// before all erase and write commands.

static FLASH_Status JF_WriteEnable( void) {
  JF.Spi.Command[0] = OPCODE_WREN;
  return (SPI_SpiReadWrite( 1, &JF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Erase a 4 KB sector and wait for the operation to complete.

static FLASH_Status JF_SectorErase( bit32 FlashAddress) { 
  if (FlashAddress % JF.SectorSize) return FLASH_ERROR;
  if (JF_WriteEnable() != FLASH_OK) return FLASH_ERROR;
  if (JF_SpiReadWriteBuffer( OPCODE_P4E, FlashAddress, 0, 0) != FLASH_OK) return FLASH_ERROR;
  return JF_WaitReady();
}

// Write a 256-byte page and wait for the operation to complete.

static FLASH_Status JF_PageWrite( bit32 FlashAddress, bptr FlashBuffer) {
  if (FlashAddress % JF.PageSize  ) return FLASH_ERROR;
  if (JF_WriteEnable() != FLASH_OK) return FLASH_ERROR;
  if (JF_SpiReadWriteBuffer( OPCODE_PP, FlashAddress, FlashBuffer, JF.PageSize) != FLASH_OK) return FLASH_ERROR;
  return JF_WaitReady();
}

// Write a block of 256-byte pages.

static FLASH_Status JF_BlockWrite( bit32 FlashAddress, bptr FlashBuffer) {
  if (FlashAddress % JF.BlockSize) return FLASH_ERROR;
  bit32 BlockOffset;
  for (BlockOffset = 0; BlockOffset < JF.BlockSize; BlockOffset += JF.PageSize) {
    if (JF_PageWrite( FlashAddress, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
    FlashBuffer  += JF.PageSize;
    FlashAddress += JF.PageSize;
  }
  return FLASH_OK;
}

// Return a non-zero code indicating the type of flash device found or
// zero if no supported device is found.

static bit8 JF_Probe( void) {
  JF.Spi.Command[0] = OPCODE_RDID;
  JF.Spi.Command[1] = 0;
  JF.Spi.Command[2] = 0;
  JF.Spi.Command[3] = 0;
  if (SPI_SpiReadWrite( 4, &JF.Spi) == SPI_OK) {
    switch (JF.Spi.Command[1]) {
      case JEDEC_00_MACRONIX:
        if ((JF.Spi.Command[2] == MX25L12835E_01) && (JF.Spi.Command[3] == MX25L12835E_02)) return MX25L12835E;
        if ((JF.Spi.Command[2] == MX25L25635E_01) && (JF.Spi.Command[3] == MX25L25635E_02)) return MX25L25635E;
        break;
  } }
  return 0;
}

// Identify the flash device and prepare it for use.

FLASH_Status JF_Init( bit32 PageSize) {
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  switch (JF.TypeCode = JF_Probe()) {
    case MX25L12835E: JF.TotalBytes = MB(16); JF.SectorSize = KB(4); JF.PageSize = 256; JF.BlockSize = KB(64); break; // 128 Mb, 16 MB
    case MX25L25635E: JF.TotalBytes = MB(32); JF.SectorSize = KB(4); JF.PageSize = 256; JF.BlockSize = KB(64); break; // 256 Mb, 32 MB
    default: return FLASH_ERROR;
  }
  return JF.BlockSize == PageSize ? FLASH_OK : FLASH_ERROR;
}

// Print the flash device identifier.

void JF_Print( void) {
  switch (JF.TypeCode) {
    case MX25L12835E: TtyPutStr( "  Macronix MX25L12835E"); break;
    case MX25L25635E: TtyPutStr( "  Macronix MX25L25635E"); break;
} }

// Erase FlashBytes (a multiple of JF.BlockSize) in flash at address FlashAddress (also a
// multiple of JF.BlockSize).

FLASH_Status JF_Erase( bit32 FlashAddress, bit32 FlashBytes) {
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if ( FlashBytes                 % JF.BlockSize ) return FLASH_ERROR;
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  while (FlashBytes) {
    if (JF_SectorErase( FlashAddress) != FLASH_OK) return FLASH_ERROR;
    if (FlashBytes > JF.SectorSize) {
      FlashBytes   -= JF.SectorSize;
      FlashAddress += JF.SectorSize;
    } else {
      FlashBytes = 0;
  } }
  return FLASH_OK;
}

// Read FlashBytes from flash at address FlashAddress.

FLASH_Status JF_Read( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer) {
  FLASH_Status Status = FLASH_OK;
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) {
    Status = FLASH_MEMORY_OVERFLOW;
  } else {
    SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
    Status = JF_SpiReadWriteBuffer( OPCODE_READ_FAST, FlashAddress, ReadBuffer, FlashBytes);
  }
  JF_AddressLength( 3);
  return Status;
}

// Verify that FlashBytes of flash at address FlashAddress (a multiple of
// JF.BlockSize) are clear (erased to 0xff).

FLASH_Status JF_Clear( bit32 FlashAddress, bit32 FlashBytes) {
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  bit32 Offset;
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if (JF_Read( FlashAddress, FlashBytes, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
  if (AllFoxes( FlashBuffer, FlashBytes, &Offset) == 0) {
    TtyPutStr( "AllFoxes failed at $");
    TtyPutHex( FlashAddress+Offset, 6);
    TtyPutLn();
    Offset &= ~0x7f;
    if (Offset > 64) Offset -= 64;
    TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
    TtyPutLn();
    return FLASH_ERROR;
  }
  return FLASH_OK;
}

// Verify FlashBytes (a multiple of JF.PageSize) from flash at address FlashAddress (also
// a multiple of JF.PageSize) match the default buffer at ADDRESS(BOARD_SDRAM_BASE).

FLASH_Status JF_Verify( bit32 FlashAddress, bit32 FlashBytes) {
  bit32 Offset;
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if ( FlashBytes                 % JF.BlockSize ) return FLASH_ERROR;
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  if (JF_Read( FlashAddress, FlashBytes, FlashBuffer) != FLASH_OK) return FLASH_ERROR; 
  if (Compare( WriteBuffer, FlashBuffer, FlashBytes, &Offset)) {
    TtyPutStr( "Verify failed at $");
    TtyPutHex( FlashAddress+Offset, 6);
    TtyPutLn();
    Offset &= ~0x7f;
    if (Offset > 64) Offset -= 64;
    TtyPutLn(); Dump( FlashAddress+Offset, WriteBuffer+Offset, 256);
    TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
    TtyPutLn();
    return FLASH_ERROR;
  }
  return FLASH_OK;
}

// Write FlashBytes (a multiple of JF.BlockSize) to flash at address FlashAddress (also a
// multiple of JF.BlockSize) from the default buffer at ADDRESS(BOARD_SDRAM_BASE).  We need to
// write a temporary copy of the data because it will be erased in-place by the low-level
// spi write operation.  If TraceBytes is nonzero, output a progress indicator as each
// block is written.

FLASH_Status JF_Write( bit32 FlashAddress, bit32 FlashBytes) {
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  MemCpy( FlashBuffer, WriteBuffer, FlashBytes);
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  while (FlashBytes) {
    if (JF_BlockWrite( FlashAddress, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
    FlashBytes   -= JF.BlockSize;
    FlashAddress += JF.BlockSize;
    FlashBuffer  += JF.BlockSize;
  }
  return FLASH_OK;
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

