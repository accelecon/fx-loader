//-------------------------------------------------------------------------------------------
//  util.c
//-------------------------------------------------------------------------------------------

#include "main.h"

static bit8 WatchdogEnabled = 1;

// After calling RestartWatchdog() you get another 15 seconds before the watchdog
// triggers.

void RestartWatchdog( void) {
  if (WatchdogEnabled) {
    MemPut32( AT91C_WDTC_WDCR, AT91C_WDTC_WDRSTT | AT91C_WDTC_KEY);
} }

// Once disabled, the watchdog can not be re-enabled.  I.E. you get exactly one
// shot at writing the WDTC_WDMR register.

void DisableWatchdog( void) {
  if (WatchdogEnabled) {
    MemPut32( AT91C_WDTC_WDMR, AT91C_WDTC_WDDIS);
    WatchdogEnabled = 0;
} }

static void DelayMsFlush( bit32 Count) {
  DelayMs( Count);
  while (TtyGetReady()) TtyGetChar(); // clear uart buffer
}

void Hang( ccptr Message) {
  DelayMsFlush( 100);
  TtyPutStr( "[*** "); 
  TtyPutStr( Message);
  TtyPutStr( "! ***]");
  for (;;) DelayRaw( 1);
}

void TtyPutChr( char c) {
  if (c == '\n') {
    TtyPutChr ('\r');
  }
  while ((MemGet32( AT91C_DBGU_CSR) & AT91C_US_TXRDY) == 0) ;
  MemPut32( AT91C_DBGU_THR, c);
}

void TtyPutStr( ccptr s) {
  while (*s) {
    TtyPutChr(*s);
    s++;
} }

void TtyPutLn( void) {
  TtyPutChr( '\n');
}

void TtyPutStrLn( ccptr s) {
  TtyPutStr( s);
  TtyPutChr( '\n');
}

bit8 TtyGetChar( void) {
  while ((MemGet32( AT91C_DBGU_CSR) & AT91C_US_RXRDY) == 0) ;
  return (bit8) (MemGet32( AT91C_DBGU_RHR) & 0xff);
}

bit8 TtyGetReady( void) {
  return (MemGet32( AT91C_DBGU_CSR) & AT91C_US_RXRDY) ? 1 : 0;
}

void TtyPutDecPad( bit32 Value, bit8 Digits, char Pad) {
  bit8 Stack[12], Depth = 0;
  if (Value == 0) {
    Stack[Depth++] = 0;
  } else {
    while (Value > 0) {
      Stack[Depth++] = (bit8) (Value % 10);
      Value /= 10;
  } }
  while (Depth < Digits) {
    TtyPutChr( Pad);
    Digits--;
  }
  while (Depth > 0) {
    TtyPutChr( '0' + Stack[--Depth]);
} }

void TtyPutDec( bit32 Value, bit8 Digits) {
  TtyPutDecPad( Value, Digits, ' ');
}

char HexDigit( bit32 Nibble) {
  Nibble &= 0x0f;
  if (Nibble < 10) {
    return Nibble + '0';
  }
  return Nibble + 'a' - 10;
}

void TtyPutHex( bit32 Value, bit8 Digits) {
  while (Digits--) {
    TtyPutChr( HexDigit( Value >> (Digits * 4)));
} }

static ccptr HexDigits = "0123456789abcdefABCDEF";

static bit8 IsHexDigit( char x) {
  bit8 i = 0;
  while (HexDigits[i]) {
    if (HexDigits[i++] == x) return 1;
  }
  return 0;
}

static bit32 HexNibble( char x) {
  if (x >= 'a') return 10 + (x - 'a');
  if (x >= 'A') return 10 + (x - 'A');
                return       x - '0' ;
}

// Get a hex environment value corresponding to the specified key.  Return zero on failure.

bit32 HexValue( ccptr String) {
  bit32 Value = 0;
  if (String) {
    if (Compare( (bptr) String, (bptr) "0x", 2, NULL) == 0) String += 2;
    while (IsHexDigit( *String)) {
      Value <<= 4;
      Value |= HexNibble( *String++);
  } }
  return Value;
}

static void DumpFlush( bit32 Address, cptr Line) {
  bit8 i;
  for (i = 0; i < 138; i++) {
    if (Line[i] == 0) break;
  }
  Line[i] = '|';
  Line[2] = ':';
  TtyPutHex( Address, 6);
  TtyPutStrLn( Line+2);
}

// Format a hex dump using lines resembling this:
//
//    74 83 f0 26 d2 8f f1 26 f2 8f f1 26 00 00 00 00 74 83 f0 26 d2 8f f1 26 f2 8f f1 26 00 00 00 00  |t..&...&...&....t..&...&...&....|
//
// We don't do run compression yet.

void Dump( bit32 FlashAddress, cbptr FlashBuffer, bit32 FlashCount) {
  TtyPutHex(         FlashAddress, 6); TtyPutChr( ' ');
  TtyPutHex( (bit32) FlashBuffer , 8); TtyPutStr( " [");
  TtyPutDec(         FlashCount  , 0); TtyPutStr( "]\n"); 
  TtyPutLn(); 
  bit32 i, j;
  char Line[140];
  for (i = 0; i < FlashCount; i++) {
    bit8 v = FlashBuffer[i], m = i % 32;
    if (m == 0) {
      if (i) {
        DumpFlush( FlashAddress, Line);
        FlashAddress += 32;
      }
      for (j = 0; j < 102; j++) {
        Line[j] = ' ';
      }
      Line[101] = '|';
      for (j = 102; j < 140; j++) {
        Line[j] = 0;
    } }
    Line[(m*3)+4] = HexDigit( v >> 4);
    Line[(m*3)+5] = HexDigit( v     );
    if ((v & 0xe0) && ! (v & 0x80)) {
      Line[102+m] = v;
    } else {
      Line[102+m] = '.';
  } }
  if (i) {
    DumpFlush( FlashAddress, Line);
  }
  TtyPutLn();
}

bit8 Compare( cbptr Buffer1, cbptr Buffer2, bit32 ByteCount, bit32 * Offset) {
  bit32 i = 0;
  for (i = 0; i < ByteCount; i++) {
    if (Buffer1[i] != Buffer2[i]) {
      if (Offset) {
        *Offset = i;
      }
      return 1; // mismatch
  } }
  return 0; // match
}

bit8 AllFoxes( cbptr Buffer, bit32 ByteCount, bit32 * Offset) {
  bit32 i = 0;
  for (i = 0; i < ByteCount; i++) {
    if (Buffer[i] != 0xff) {
      if (Offset) {
        *Offset = i;
      }
      return 0; // mismatch
  } }
  return 1; // match
}

void MemSet( bptr Target, bit8 Source, bit32 ByteCount) {
  while (ByteCount--) {
    *Target++ = Source;
} }

void MemCpy( bptr Target, cbptr Source, bit32 ByteCount) {
  while (ByteCount--) {
    *Target++ = *Source++;
} }

bit32 StrLen( ccptr String) {
  bit32 Length = 0;
  if (String) {
    while (*String++) {
      Length++;
  } }
  return Length;
}

cptr StrCpy( cptr Target, ccptr Source) {
  bit8 i = 0;
  while ((Target[i] = Source[i])) {
    i++;
  }
  return Target;
}

bit8 StrCmp( ccptr Target, ccptr Source) {
  bit8 i = 0;
  while (Target[i] && ((Target[i] == Source[i]))) {
    i++;
  }
  return Target[i] - Source[i];
}

// CRC16 implementation according to CCITT standards

const bit16 CrcTable[256]= {
	0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
	0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
	0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
	0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
	0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
	0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
	0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
	0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
	0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
	0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
	0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
	0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
	0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
	0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
	0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
	0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
	0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
	0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
	0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
	0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
	0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
	0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
	0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
	0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
	0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
	0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
	0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
	0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
	0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
	0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
	0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
	0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

bit16 Crc16( bptr Buffer, bit32 Length) {
	bit32 Count;
  bit16 Crc = 0;
	for (Count = 0; Count < Length; Count++) {
		Crc = (Crc << 8) ^ CrcTable[((Crc >> 8) ^ *Buffer++) & 0x00FF];
  }
	return Crc;
}

const cptr StrXmodem = "RecvXmodem ";

const cptr StrFlash  = "Flash_";
const cptr StrRead   = "Read"  ;
const cptr StrWrite  = "Write" ;
const cptr StrVerify = "Verify";
const cptr StrErase  = "Erase" ;
const cptr StrClear  = "Clear" ;
const cptr StrInit   = "Init"  ;
const cptr StrPage   = "Page"  ;

#define XMODEM_SOH  0x01 // for  128-byte blocks
#define XMODEM_STX  0x02 // for 1024-byte blocks
#define XMODEM_EOT  0x04
#define XMODEM_ACK  0x06
#define XMODEM_QRY  'C'

static bit32 RecvBytes = 0;
static bit16 RecvBlock = 0;
static bit8  RecvCode  = 0;
static bit8  RecvChk1  = 0;
static bit8  RecvChk2  = 0;
static bit16 RecvCrcX  = 0;
static bit16 RecvCrcY  = 0;
static bit8  RecvCrc1  = 0;
static bit8  RecvCrc2  = 0;
static bptr  RecvBuff  = 0;

static bit32 RecvXmodem( ccptr BinFile, bit32 Offset) {
  bit16 Packet = 1;
  RecvBuff = ADDRESS(BOARD_SDRAM_BASE) + Offset;
  TtyPutLn();
  TtyPutStr( StrXmodem);
  TtyPutStr( BinFile);
  TtyPutStr( ".bin...\n");
  DelayMsFlush( 5000);
  TtyPutChr( XMODEM_QRY);
  RecvBytes = 0;
  while (1) {
    if (TtyGetReady()) {
      RecvBlock = 0;
      switch (RecvCode = TtyGetChar()) {
        case XMODEM_SOH: RecvBlock =  128; break;
        case XMODEM_STX: RecvBlock = 1024; break;
        case XMODEM_EOT: TtyPutChr( XMODEM_ACK); return 0;
        default: return 1;
      }
      if (RecvBlock) {
        RecvChk1 = TtyGetChar();
        RecvChk2 = TtyGetChar() ^ 0xFF;
        if ((RecvChk1 == RecvChk2) && (RecvChk1 == Packet)) {
          bit16 i;
          for (i = 0; i < RecvBlock + 2; i++) {
            RecvBuff[i] = TtyGetChar();
          }
          RecvCrc1 = RecvBuff[RecvBlock  ];
          RecvCrc2 = RecvBuff[RecvBlock+1];
          RecvCrcX = Crc16( RecvBuff, RecvBlock);
          RecvCrcY = (RecvCrc1 << 8) | RecvCrc2;
          if (RecvCrcX != RecvCrcY) {
            return 4;
          }
          Packet = (Packet + 1) & 0xFF;
          RecvBuff  += RecvBlock;
          RecvBytes += RecvBlock;
          TtyPutChr( XMODEM_ACK);
        } else {
          return 2;
} } } } }

// Receive a file from the debug console using Xmodem protocol into a buffer starting
// at BOARD_SDRAM_BASE[Offset].

static bit8 RecvFile( ccptr BinFile, bit32 Offset) {
  bit8 RecvError = RecvXmodem( BinFile, Offset);
  DelayMsFlush( 100);
  if (RecvError) {
    TtyPutStr( "\n### RecvError ");
    TtyPutDec( RecvError, 0); TtyPutChr( ' ');
    TtyPutDec( RecvCode , 0); TtyPutChr( ' ');
    TtyPutDec( RecvBlock, 0);
    TtyPutLn();    
    return 0; // failure
  }
  TtyPutStr( StrXmodem); TtyPutDec( RecvBytes, 0); TtyPutLn();
  if (AllFoxes( ADDRESS(BOARD_SDRAM_BASE)+Offset, RecvBytes, 0)) {
    TtyPutStrLn( "All 0xff!");
  }
  return 1; // success
}

static void TtyPutBytesPercent( cptr Type, bit32 Bytes, bit32 Percent) {
  TtyPutStr( "Partition ");
  TtyPutStr( Type);
  TtyPutStr( ": ");
  TtyPutDec( Bytes, 0);
  TtyPutStr( " bytes, ");
  TtyPutDec( Percent, 0);
  TtyPutStrLn( "%");
}

static void TtyPutElipsis( void) {
  TtyPutStr( "...");
}

void TtyPutFlash( cptr Routine) {
  TtyPutStr( StrFlash);
  TtyPutStr( Routine);
  TtyPutElipsis();
}

void TtyPutOk( void) {
  TtyPutStrLn( "OK");
}

void TtyPutBad( void) {
  TtyPutStrLn( "BAD");
}

// Blow away a region by erasing bytes in the JedecFlash.

void ZapJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks) {
  TtyPutLn();
  TtyPutStr( "Zap ");
  TtyPutStr( BinFile);
  TtyPutStr( "...\n");
  TtyPutFlash( StrInit);
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) {
    TtyPutOk();
    bit32 EraseBytes = FlashBlocks * JF_BLOCKSIZE;
    TtyPutBytesPercent( StrErase, EraseBytes, 100);
    TtyPutFlash( StrErase);
    if (JF_Erase( FlashAddress, EraseBytes) == FLASH_OK) {
      TtyPutOk();
      TtyPutFlash( StrClear);
      if (JF_Clear( FlashAddress, EraseBytes) == FLASH_OK) {
        TtyPutOk();
      } else {
        TtyPutBad();
      }
    } else {
      TtyPutBad();
    }
  } else {
    TtyPutBad();
} }

// Test a region by writing a pattern in the JedecFlash.

void TestJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks) {
  TtyPutLn();
  TtyPutStr( "Test ");
  TtyPutStr( BinFile);
  TtyPutStr( "...\n");
  TtyPutFlash( StrInit);
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) {
    TtyPutOk();
    bit32 EraseBytes = FlashBlocks * JF_BLOCKSIZE;
    TtyPutBytesPercent( StrErase, EraseBytes, 100);
    TtyPutFlash( StrErase);
    if (JF_Erase( FlashAddress, EraseBytes) == FLASH_OK) {
      TtyPutOk();
      TtyPutFlash( StrClear);
      if (JF_Clear( FlashAddress, EraseBytes) == FLASH_OK) {
        int i, j = 0;
        TtyPutOk();
        for (i = 0; i < EraseBytes; i++, j++) {
          ADDRESS(BOARD_SDRAM_BASE)[i] = j;
          if (j == 255) {
            j = 0;
        } }
        TtyPutFlash( StrWrite);
        if (JF_Write( FlashAddress, EraseBytes) == FLASH_OK) {
          TtyPutOk();
          TtyPutFlash( StrVerify);
          if (JF_Verify( FlashAddress, EraseBytes) == FLASH_OK) {
            TtyPutOk();
          } else {
            TtyPutBad();
          }
        } else {
          TtyPutBad();
        }
      } else {
        TtyPutBad();
      }
    } else {
      TtyPutBad();
    }
  } else {
    TtyPutBad();
} }

// Check the header and integrity of a region by loading an image in the JedecFlash.

void CheckJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType) {
  TtyPutLn();
  TtyPutStr( "Check ");
  TtyPutStr( BinFile);
  TtyPutStr( "...\n");
  TtyPutFlash( StrInit);
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) {
    TtyPutOk();
    bptr ReadBuffer = ADDRESS(BOARD_SDRAM_BASE);
    bit32 PartitionBytes = FlashBlocks * JF_BLOCKSIZE;
    if (ImageType == e_Loader) {
      bit32 LoaderEnvBytes = JF_BLOCKS_UBENV * JF_BLOCKSIZE;
      bit32 LoaderEnvAddress = FlashAddress + PartitionBytes - LoaderEnvBytes;
      if (!uBootEnvList( LoaderEnvAddress, LoaderEnvBytes, ReadBuffer)) {
        uBootEnvReset( LoaderEnvAddress, LoaderEnvBytes, 1);
    } }
    if (ImageType == e_uBoot) {
      uBootCheckImage( FlashAddress, PartitionBytes, ReadBuffer, e_Verbose);
    }
    if (ImageType == e_uEnv) {
      uBootEnvList( FlashAddress, PartitionBytes, ReadBuffer);
    }
    if (ImageType == e_uImage) {
      TtyPutFlash( StrRead);
      if (JF_Read( FlashAddress, uImageHeaderSize, ReadBuffer) == FLASH_OK) {
        TtyPutOk();
        p_uImageHeader Header = (p_uImageHeader) ReadBuffer;
        if (uImageCheckHeader( Header, 0, e_Verbose)) {
          bptr Data = ReadBuffer + uImageHeaderSize;
          TtyPutFlash( StrRead);
          if (JF_Read( FlashAddress + uImageHeaderSize, Header->DataSize, Data) == FLASH_OK) {
            TtyPutOk();
            uImageCheckData( Header, Data);
          } else {
            TtyPutBad();
        } }
      } else {
        TtyPutBad();
    } }
  } else {
    TtyPutBad();
} }

// Receive an Xmodem image into RAM at ADDRESS(BOARD_SDRAM_BASE) and write it to JedecFlash.  After
// the write, read it back and verify the accuracy of the write.

void XmodemToJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType) {
  TtyPutFlash( StrInit);
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) {
    TtyPutOk();
    bit32 Offset = 0;
    if (RecvFile( BinFile, Offset)) {
      bit32 PartitionBytes = FlashBlocks * JF_BLOCKSIZE;
      bit32 WriteBytes = (((RecvBytes - 1) / JF_BLOCKSIZE) + 1) * JF_BLOCKSIZE;
      TtyPutBytesPercent( StrWrite, WriteBytes, ((WriteBytes / JF_BLOCKSIZE) * 100) / FlashBlocks);
      if (ImageType == e_Loader) {
        TtyPutStr( "Loader_Image...");
        bit32 TotalFlashBytes = JF_BLOCKS_TOTAL * JF_BLOCKSIZE;
        if (((RecvBytes > KB(10)) && (RecvBytes <= KB(28))) ||  // at least 10 KB but fits in SDRAM (28KB)
            ( RecvBytes > (TotalFlashBytes - JF_BLOCKSIZE))) {  // or fills entire flash (pretty much)
          TtyPutOk();
        } else {
          TtyPutBad();
          if (LoaderProtect) {
            return;
      } } }
      if (ImageType == e_uBoot) {
        p_UbootHeader Header = (p_UbootHeader) ADDRESS(BOARD_SDRAM_BASE);
        TtyPutStr( "uBoot_Image...");
        if (uBootValidHeader( Header, RecvBytes, PartitionBytes)) {
          uBootStampHeader( Header, RecvBytes);
          TtyPutOk();
        } else {
          TtyPutBad();
          return;
      } }
      if (ImageType == e_uEnv) {
        bit32 n;
        bptr Header = (bptr) ADDRESS(BOARD_SDRAM_BASE);
        TtyPutStr( "uBoot_Environment...");
        for (n = 4; n < RecvBytes; n++) {
          if ((Header[n] == 0) && (Header[n+1] == 0)) {
            for (n = n+2; n < PartitionBytes; n++) {
              Header[n] = 0xff;
            }
            break;
        } }
        TtyPutOk();
      }
      if (ImageType == e_uImage) {
        p_uImageHeader Header = (p_uImageHeader) ADDRESS(BOARD_SDRAM_BASE);
        TtyPutStr( "uImage...");
        if (uImageCheckHeader( uImageCopyHeader( Header), 0, e_Silent)) {
          TtyPutOk();
        } else {
          TtyPutBad();
          TtyPutStr( "Warning: not a valid uImage!\n");
      } }
      TtyPutFlash( StrErase);
      if (JF_Erase( FlashAddress, WriteBytes) == FLASH_OK) {
        TtyPutOk();
        TtyPutFlash( StrClear);
        if (JF_Clear( FlashAddress, WriteBytes) == FLASH_OK) {
          TtyPutOk();
          TtyPutFlash( StrWrite);
          if (JF_Write( FlashAddress, WriteBytes) == FLASH_OK) {
            TtyPutOk();
            TtyPutFlash( StrVerify);
            if (JF_Verify( FlashAddress, WriteBytes) == FLASH_OK) {
              TtyPutOk();
            } else {
              TtyPutBad();
            }
          } else {
            TtyPutBad();
          }
        } else {
          TtyPutBad();
        }
      } else {
        TtyPutBad();
    } }
  } else {
    TtyPutBad();
} }

void TtyPutFlashGeometry( bit32 Blocks, bit32 BlockSize) {
  TtyPutDec(          BlockSize,  8);
  TtyPutDec( Blocks            ,  7);
  TtyPutDec( Blocks * BlockSize, 10);
  TtyPutStrLn( " bytes");
}

void TtyPutFailure( cptr Routine) {
  TtyPutFlash( Routine);
  TtyPutBad();
}

bit32 JedecFlash_Offset = 0;

void DumpJedecFlash( void) {
  bit32 BlockSize = (JF_BLOCKSIZE > KB(1)) ? KB(1) : JF_BLOCKSIZE;
  TtyPutLn();
  switch (JF_Init( JF_BLOCKSIZE)) {
    case FLASH_OK:
      if (JF_Read( JedecFlash_Offset, BlockSize, ADDRESS(BOARD_SDRAM_BASE)) == FLASH_OK) {
        Dump( JedecFlash_Offset, ADDRESS(BOARD_SDRAM_BASE), BlockSize);
        JedecFlash_Offset += BlockSize;
      } else {
        TtyPutFailure( StrRead);
      }
      break;
    case FLASH_BAD_PAGESIZE:
      TtyPutFailure( StrPage);
      break;
    default:
      TtyPutFailure( StrInit);
} }

void DisplayMenuItem( char MenuSelection, char Partition, cptr BinName, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType) {
  bit32 PartitionBytes = FlashBlocks * JF_BLOCKSIZE;
  TtyPutChr( MenuSelection);
  TtyPutChr( (Partition == MenuSelection) ? '=' : ':');
  TtyPutChr( ' ');
  TtyPutStr( BinName);
  if (StrLen( BinName) < 8) TtyPutChr( ' ');
  if (StrLen( BinName) < 7) TtyPutChr( ' ');
  if (StrLen( BinName) < 6) TtyPutChr( ' ');
  TtyPutStr( "  $");
  TtyPutHex( FlashAddress + JF_ADDR_BASE, 8);
  TtyPutStr( "..");
  TtyPutHex( (FlashAddress + JF_ADDR_BASE) + (FlashBlocks * JF_BLOCKSIZE) - 1, 8);
  TtyPutChr(' ');
  TtyPutDec( FlashBlocks, 4);
  TtyPutDec( PartitionBytes, 10);
  if (ImageType == e_Loader) {
    bit32 LoaderEnvBytes = JF_BLOCKS_UBENV * JF_BLOCKSIZE;
    bit32 LoaderEnvAddress = FlashAddress + PartitionBytes - LoaderEnvBytes;
    uBootEnvCheck( LoaderEnvAddress, LoaderEnvBytes, ADDRESS(BOARD_SDRAM_BASE), e_Quiet);
  }
  if (ImageType == e_uBoot) {
    uBootCheckImage( FlashAddress, PartitionBytes, ADDRESS(BOARD_SDRAM_BASE), e_Quiet);
  }
  if (ImageType == e_uEnv) {
    uBootEnvCheck( FlashAddress, PartitionBytes, ADDRESS(BOARD_SDRAM_BASE), e_Quiet);
  }
  if (ImageType == e_uImage) {
    bptr ReadBuffer = ADDRESS(BOARD_SDRAM_BASE);
    if (JF_Read( FlashAddress, uImageHeaderSize, ReadBuffer) == FLASH_OK) {
      uImageCheckHeader( (p_uImageHeader) ReadBuffer, PartitionBytes, e_Quiet);
  } }
  TtyPutLn();
}

// For 48.000 MHz BOARD_MAIN_CLOCK, good (zero error) baud rates are 115200, 230400,
// 500k, 1000k, 1500k and 3000k.  Set baud rate to either SLOW or FAST, and return
// the Fast flag.

// For 133.333 MHz BOARD_MAIN_CLOCK, good (zero error) baud rates are 115200, 230400,
// 500k, 1000k, 1500k and 3000k.  Set baud rate to either SLOW or FAST, and return
// the Fast flag.

#define BAUDRATE_SLOW   115200
#define BAUDRATE_FAST  1000000

bit8 TtyConfig( bit8 Fast, bit8 Announce) {
  bit32 BaudRate = Fast ? BAUDRATE_FAST : BAUDRATE_SLOW;
  bit32 BaudRateDivisor = MasterClock       / (BaudRate * 16)      ;
  bit32 BaudRateError = ((MasterClock * 10) / (BaudRate * 16)) % 10;
  if (BaudRateError > 4) {
    BaudRateDivisor += 1;
  }
  if (Announce) {
    TtyPutStr( "\nBaud rate is now ");
    TtyPutDec( BaudRate, 1);
    TtyPutStrLn( "...");
    DelayMs( 100);
  }
  MemPut32( AT91C_PIOA_PDR, AT91C_PA10_DTXD | AT91C_PA9_DRXD);
  MemPut32( AT91C_DBGU_IDR, ~0);
  MemPut32( AT91C_DBGU_CR, AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS);
  MemPut32( AT91C_DBGU_BRGR, BaudRateDivisor);
  MemPut32( AT91C_DBGU_MR, AT91C_US_PAR_NONE);
  MemPut32( AT91C_DBGU_CR, AT91C_US_RXEN | AT91C_US_TXEN);
  return Fast;
}

// reset the cpu

void Reset( void) {
  MemPut32( AT91C_BASE_RSTC, AT91C_RSTC_KEY | AT91C_RSTC_EXTRST | AT91C_RSTC_PROCRST | AT91C_RSTC_PERRST);
	while (1);
}

void __div0                    ( void) { Hang( "DBZ"); }
void Trap_undefined_instruction( void) { Hang( "UIT"); }
void Trap_software_interrupt   ( void) { Hang( "SIT"); }
void Trap_prefetch_abort       ( void) { Hang( "PAT"); }
void Trap_data_abort           ( void) { Hang( "DAT"); }
void Trap_irq                  ( void) { Hang( "IQT"); }
void Trap_fiq                  ( void) { Hang( "FQT"); }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

