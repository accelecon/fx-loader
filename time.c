//-------------------------------------------------------------------------------------------
//  time.c
//-------------------------------------------------------------------------------------------

#include "main.h"

#define BASE_YEAR                        2011
#define BASE_TIME  1293858000  // 01-Jan-2011 00:00:00

#define YEAR_MAX  16

static bit32 YearDay[YEAR_MAX] = {
     0, // Sat Jan 1 2011
   365, // Sun Jan 1 2012 *
   731, // Tue Jan 1 2013
  1096, // Wed Jan 1 2014
  1461, // Thu Jan 1 2015
  1826, // Fri Jan 1 2016 *
  2192, // Sun Jan 1 2017
  2557, // Mon Jan 1 2018
  2922, // Tue Jan 1 2019
  3287, // Wed Jan 1 2020 *
  3653, // Fri Jan 1 2021
  4018, // Sat Jan 1 2022
  4383, // Sun Jan 1 2023
  4748, // Mon Jan 1 2024 *
  5114, // Wed Jan 1 2025
  5479  // Thu Jan 1 2026
};

static bit32 MonthDays[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define SECS_PER_DAY (60*60*24)

// Like: Wed Dec 07 2011 01:04:45

static void PrintTime( bit32 Year, bit32 Month, bit32 MonthDay, bit32 WeekDay, bit32 Hour, bit32 Mins, bit32 Secs) {
  static char MonthName  [] = "JanFebMarAprMayJunJulAugSepOctNovDec";
  static char WeekDayName[] = "SunMonTueWedThuFriSat";
  WeekDay *= 3;
  Month   *= 3;
  TtyPutChr( WeekDayName[WeekDay++]);
  TtyPutChr( WeekDayName[WeekDay++]);
  TtyPutChr( WeekDayName[WeekDay  ]); TtyPutChr( ' ');
  TtyPutChr( MonthName[Month++]);
  TtyPutChr( MonthName[Month++]);
  TtyPutChr( MonthName[Month  ]); TtyPutChr( ' ');
  TtyPutDecPad( MonthDay+1, 2, '0'); TtyPutChr( ' ');
  TtyPutDecPad( Year, 4, '0'); TtyPutChr( ' ');
  TtyPutDecPad( Hour, 2, '0'); TtyPutChr( ':');
  TtyPutDecPad( Mins, 2, '0'); TtyPutChr( ':');
  TtyPutDecPad( Secs, 2, '0');
}

#define LIST(x,y) TtyPutStr( x); TtyPutStr( ": "); TtyPutDec( y, 1); TtyPutLn();

void TtyPutTime( bit32 Time) {
  Time -= BASE_TIME;
  bit32 Secs = Time % SECS_PER_DAY;
  bit32 Mins = Secs / 60;
        Secs = Secs % 60;
  bit32 Hour = Mins / 60;
        Mins = Mins % 60;
  bit32 Days = Time / SECS_PER_DAY;
  bit32 WeekDay = (Days + 6) % 7;
  bit32 Year;
  for (Year = 0; Year < YEAR_MAX; Year++) {
    if (YearDay[Year] >= Days) {
      Year -= 1;
      break;
  } }
  if (Year < YEAR_MAX) {
    bit32 MonthDay = Days - YearDay[Year], Month;
    Year += BASE_YEAR;
    for (Month = 0; Month < 12; Month++) {
      bit32 LeapDay = 0;
      if ((Month == 1) && ((Year % 4) == 0)) {
        LeapDay = 1;
      }
      if (MonthDay < (MonthDays[Month] + LeapDay)) {
        PrintTime( Year, Month, MonthDay, WeekDay, Hour, Mins, Secs); 
        return;
      }
      MonthDay -= MonthDays[Month] + LeapDay;
  } }
  PrintTime( 1999, 0, 0, 0, 0, 0, 0);
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

