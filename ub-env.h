//-------------------------------------------------------------------------------------------
//  ub-env.h
//-------------------------------------------------------------------------------------------

extern bool uBootEnvCheck( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, e_DisplayLevel DisplayLevel);
extern bool uBootEnvList ( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer);
extern void uBootEnvReset( bit32 FlashAddress, bit32 PartitionBytes, bool Force);
extern bool uBootEnvSave ( bit32 FlashAddress, bit32 PartitionBytes);

extern ccptr uBootEnvStr( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, ccptr Name);
extern bit32 uBootEnvHex( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, ccptr Name);

extern ccptr uBootEnvSerialGet( ccptr EthAddr);
extern void  uBootEnvSerialEditLcd( void);
extern bool  uBootEnvSerialEditTty( void);

extern void uBootFileSysSelect( ccptr Name, char BootPart, char Mtd, ccptr Address);

#define UBENV_BYTES (JF_BLOCKS_UBENV * JF_BLOCKSIZE)

// We assume LOADER is immediately followed by the UBOOT to simplify the math below.

#define GetEnvString(x)  uBootEnvStr( JF_ADDR_UBENV              , UBENV_BYTES, ADDRESS(BOARD_SDRAM_BASE), x)
#define GetLdrString(x)  uBootEnvStr( JF_ADDR_UBOOT - UBENV_BYTES, UBENV_BYTES, ADDRESS(BOARD_SDRAM_BASE), x)

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

