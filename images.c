//-------------------------------------------------------------------------------------------
//  images.c
//-------------------------------------------------------------------------------------------

//  Support for manipulation and validation of u-boot, uImage and zImage images.

#include "main.h"

#define   UBOOT_MAGIC  0xDEADBEEF
#define  UIMAGE_MAGIC  0x27051956
#define  ZIMAGE_MAGIC  0x016F2818

static bit16 Swap16( bit16 x) {
  return ((x >> 8) & 0xff) | ((x & 0xff) << 8);
}

static bit32 Swap32( bit32 x) {
  return Swap16((x >> 16) & 0xffff) | (Swap16( x & 0xffff) << 16);
}

//============================================================================
//  U-Boot environment crc32 lookup table.
//============================================================================

static const bit32 uImageCrcTable[256] = {
  0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L, 0x706af48fL, 0xe963a535L, 0x9e6495a3L,
  0x0edb8832L, 0x79dcb8a4L, 0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L, 0x90bf1d91L,
  0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL, 0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L,
  0x136c9856L, 0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L, 0xfa0f3d63L, 0x8d080df5L,
  0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L, 0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
  0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L, 0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L,
  0x26d930acL, 0x51de003aL, 0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L, 0xb8bda50fL,
  0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L, 0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL,
  0x76dc4190L, 0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL, 0x9fbfe4a5L, 0xe8b8d433L,
  0x7807c9a2L, 0x0f00f934L, 0x9609a88eL, 0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
  0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL, 0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L,
  0x65b0d9c6L, 0x12b7e950L, 0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L, 0xfbd44c65L,
  0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L, 0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL,
  0x4369e96aL, 0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L, 0xaa0a4c5fL, 0xdd0d7cc9L,
  0x5005713cL, 0x270241aaL, 0xbe0b1010L, 0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
  0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L, 0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL,
  0xedb88320L, 0x9abfb3b6L, 0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L, 0x73dc1683L,
  0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L, 0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L,
  0xf00f9344L, 0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL, 0x196c3671L, 0x6e6b06e7L,
  0xfed41b76L, 0x89d32be0L, 0x10da7a5aL, 0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
  0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L, 0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL,
  0xd80d2bdaL, 0xaf0a1b4cL, 0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL, 0x4669be79L,
  0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L, 0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL,
  0xc5ba3bbeL, 0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L, 0x2cd99e8bL, 0x5bdeae1dL,
  0x9b64c2b0L, 0xec63f226L, 0x756aa39cL, 0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
  0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL, 0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L,
  0x86d3d2d4L, 0xf1d4e242L, 0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L, 0x18b74777L,
  0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL, 0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L,
  0xa00ae278L, 0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L, 0x4969474dL, 0x3e6e77dbL,
  0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L, 0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
  0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L, 0xcdd70693L, 0x54de5729L, 0x23d967bfL,
  0xb3667a2eL, 0xc4614ab8L, 0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL, 0x2d02ef8dL
};

#define DO1(x) Crc = uImageCrcTable[((int)Crc ^ (*x++)) & 0xff] ^ (Crc >> 8);

#define DO2(x) DO1(x); DO1(x);
#define DO4(x) DO2(x); DO2(x);
#define DO8(x) DO4(x); DO4(x);

//============================================================================
//  U-Boot crc32 calculation.
//============================================================================

bit32 uImageCrc32( bit32 Crc, bptr Buffer, bit32 Length) {
  Crc ^= 0xffffffffL;
  while (Length >= 8) {
    DO8( Buffer);
    Length -= 8;
  }
  if (Length) { 
    do {
      DO1( Buffer);
    } while (--Length);
  }
  return Crc ^ 0xffffffffL;
}

//============================================================================
//  Return a pointer to a static copy of an image header so we can manipulate
//  it without damaging the original header.
//============================================================================

p_uImageHeader uImageCopyHeader( p_uImageHeader Header) {
  static s_uImageHeader Header2;
  MemCpy( (bptr) &Header2, (bptr) Header, uImageHeaderSize);
  return &Header2;
}

//============================================================================
//  Check the integrity of the header segment of a uImage.  Return 1 on
//  success, zero on failure.  Verbosity levels can be e_Silent, e_Quiet or
//  e_Verbose.  Header is modified in-place and upon return is in native
//  byte ordering.
//============================================================================

bit8 uImageCheckHeader( p_uImageHeader Header, bit32 PartitionBytes, e_DisplayLevel DisplayLevel) {
  bit32 SwapHeaderMagic = Swap32( Header->HeaderMagic), SwapHeaderCrc;
  if (DisplayLevel == e_Verbose) {
    TtyPutStrLn( "uImage:");
    TtyPutStr( "  Magic  . . . . . . $"); TtyPutHex( SwapHeaderMagic, 8);
  }
  if (SwapHeaderMagic == UIMAGE_MAGIC) {
    bit32 CheckHeaderCrc;
    if (DisplayLevel == e_Verbose) {
      TtyPutStrLn( " (ok)");
    }
    SwapHeaderCrc = Swap32( Header->HeaderCrc);
    Header->HeaderCrc = 0;
    CheckHeaderCrc = uImageCrc32( 0, (bptr) Header, uImageHeaderSize);
    Header->HeaderMagic = SwapHeaderMagic;
    Header->HeaderCrc   = SwapHeaderCrc  ;
    Header->DataSize    = Swap32( Header->DataSize  );
    Header->CreateTime  = Swap32( Header->CreateTime);
    Header->DataLoad    = Swap32( Header->DataLoad  );
    Header->EntryPoint  = Swap32( Header->EntryPoint);
    Header->DataCrc     = Swap32( Header->DataCrc   );
    Header->ImageName[31] = 0;
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( "  Image Name . . . . " ); TtyPutStrLn( Header->ImageName);
      TtyPutStr( "  Created  . . . . . " ); TtyPutTime( Header->CreateTime); TtyPutLn();
      TtyPutStr( "  Data Size  . . . . $"); TtyPutHex( Header->DataSize  , 8); TtyPutStr( " ("); TtyPutDec( Header->DataSize, 1); TtyPutStrLn( ")");
      TtyPutStr( "  Load Address . . . $"); TtyPutHex( Header->DataLoad  , 8); TtyPutLn();
      TtyPutStr( "  Entry Point  . . . $"); TtyPutHex( Header->EntryPoint, 8); TtyPutLn();
      TtyPutStr( "  Data CRC . . . . . $"); TtyPutHex( Header->DataCrc   , 8); TtyPutLn();
      TtyPutStr( "  System Type  . . . $"); TtyPutHex( Header->SystemType, 2); TtyPutLn();
      TtyPutStr( "  Arch Type  . . . . $"); TtyPutHex( Header->ArchType  , 2); TtyPutLn();
      TtyPutStr( "  Image Type . . . . $"); TtyPutHex( Header->ImageType , 2); TtyPutLn();
      TtyPutStr( "  Comp Type  . . . . $"); TtyPutHex( Header->CompType  , 2); TtyPutLn();
      TtyPutStr( "  Header CRC . . . . $"); TtyPutHex( Header->HeaderCrc , 8);
    }
    if (DisplayLevel == e_Quiet) {
      TtyPutStr( ", uImage: "); TtyPutStr( Header->ImageName);
      TtyPutStr( ", "); TtyPutTime( Header->CreateTime);
      TtyPutStr( ", "); TtyPutDec( Header->DataSize, 1);
    }
    if (Header->HeaderCrc == CheckHeaderCrc) {
      if (DisplayLevel != e_Silent) {
        TtyPutStr( " (ok");
        if (PartitionBytes) {
          TtyPutStr( ", ");
          TtyPutDec( ((Header->DataSize + uImageHeaderSize) * 100) / PartitionBytes, 0);
          TtyPutChr( '%');
        }
        TtyPutChr( ')');
      }
      if (DisplayLevel == e_Verbose) {
        TtyPutLn();
      }
      return 1; // success
    }
    if (DisplayLevel != e_Silent) {
      TtyPutStr( " (bad, calculated $");
      TtyPutHex( CheckHeaderCrc, 8);
    }
    if (DisplayLevel == e_Quiet) {
      TtyPutChr( ')');
    }
  } else {
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( " (bad, expected $");
      TtyPutHex( UIMAGE_MAGIC, 8);
  } }
  if (DisplayLevel == e_Verbose) {
    TtyPutStrLn( ")");
  }
  return 0; // failure
}

//============================================================================
//  Check the integrity of the data segment of a linux image.  Return 1 on
//  success, zero on failure.  This is designed to be done immediaely after
//  a call to CheckLinuxHeader with a DisplayLevel of e_Verbose.
//============================================================================

bit8 uImageCheckData( p_uImageHeader Header, bptr Data) {
  TtyPutStr( "  Data Size  . . . . $"); TtyPutHex( Header->DataSize, 8); TtyPutStr( " ("); TtyPutDec( Header->DataSize, 1); TtyPutStrLn( ")");
  TtyPutStr( "  Data CRC . . . . . $"); TtyPutHex( Header->DataCrc , 8);
  if (Header->DataSize < MB(8)) {
    bit32 CheckDataCrc = uImageCrc32( 0, Data, Header->DataSize);
    if (CheckDataCrc == Header->DataCrc) {
      TtyPutStrLn( " (ok)");
      return 1; // success
    } 
    TtyPutStr( " (bad, calculated $");
    TtyPutHex( CheckDataCrc, 8);
  } else {
    TtyPutStr( " (bad, length");
  }
  TtyPutStrLn( ")");
  return 0; // failure
}

//============================================================================
//  Check if a freshly-uploaded (and not-yet-stamped) u-boot header looks
//  kosher.
//============================================================================

bit8 uBootValidHeader( p_UbootHeader Header, bit32 ImageBytes, bit32 PartitionBytes) {
  if (ImageBytes <= PartitionBytes) {
    if (ImageBytes > UbootHeaderSize) {
       if (Header->DataCrc == UBOOT_MAGIC) {
        return 1; // success;
  } } }
  return 0; // failure
}

//============================================================================
//  Plug the length and crc into a u-boot image header.  We use two benign
//  32-bit words in the first 64 bytes of the u-boot image to store a length
//  and crc which will subsequently validate the data portion of the image.
//============================================================================

void uBootStampHeader( p_UbootHeader Header, bit32 ImageBytes) {
  bptr Data = ((bptr) Header) + UbootHeaderSize;
  Header->DataSize = ImageBytes - UbootHeaderSize;
  Header->DataCrc = uImageCrc32( 0, Data, Header->DataSize);
}

//============================================================================
//  Check the integrity of a stamped u-boot image.  We use two benign 32-bit
//  words in the first 64 bytes of the u-boot image to retrieve a length and
//  crc which we use to validate the data portion of the u-boot image.
//============================================================================

// e_Silent: emit nothing
// e_Quiet: emit summary without a final newline
// e_Verbose: emit lots, with a final newline

bit8 uBootCheckImage( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, e_DisplayLevel DisplayLevel) {
  bit8 UbootImageValid = 0;
  bit32 HeaderBytes = UbootHeaderSize;
  p_UbootHeader Header = (p_UbootHeader) ADDRESS(BOARD_SDRAM_BASE);
  if (JF_Read( FlashAddress, HeaderBytes, (bptr) Header) == FLASH_OK) {
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( "  Data Size  . . . . $"); TtyPutHex( Header->DataSize  , 8); TtyPutStr( " ("); TtyPutDec( Header->DataSize, 1); TtyPutStrLn( ")");
      TtyPutStr( "  Data CRC . . . . . $"); TtyPutHex( Header->DataCrc, 8);
    }
    if ((Header->DataSize               <  PartitionBytes) &&
        (Header->DataSize + HeaderBytes <= PartitionBytes)) {
      bptr Data = ((bptr) Header) + HeaderBytes;
      if (JF_Read( FlashAddress + HeaderBytes, Header->DataSize, Data) == FLASH_OK) {
        bit32 CheckCrc = uImageCrc32( 0, Data, Header->DataSize);
        if (CheckCrc == Header->DataCrc) {
          UbootImageValid = 1;
          if (DisplayLevel != e_Silent) {
            TtyPutStr( " (ok");
            TtyPutStr( ", ");
            TtyPutDec( ((Header->DataSize + uImageHeaderSize) * 100) / PartitionBytes, 0);
            TtyPutStr( "%)");
            if (DisplayLevel == e_Verbose) {
              TtyPutLn();
          } }
        } else {
          if (DisplayLevel == e_Verbose) {
            TtyPutStr( " (bad, calculated $");
            TtyPutHex( CheckCrc, 8);
            TtyPutStrLn( ")");
        } }
      } else {
        TtyPutStr( " (bad, read failure)");
        if (DisplayLevel == e_Quiet) {
          return 0; // failure
        }
        TtyPutLn();
      } 
    } else {
      if (DisplayLevel == e_Verbose) {
        TtyPutStrLn( " (bad, invalid length)");
  } } }
  if (UbootImageValid) {
    return 1; // success
  }
  if (DisplayLevel == e_Quiet) {
    TtyPutStr( " (bad)");
  }
  return 0; // failure
}

//============================================================================
//  zImage support
//============================================================================

// list of possible tags

#define ATAG_NONE       0x00000000
#define ATAG_CORE       0x54410001
#define ATAG_MEM        0x54410002
#define ATAG_RAMDISK    0x54410004
#define ATAG_INITRD2    0x54420005
#define ATAG_SERIAL     0x54410006
#define ATAG_REVISION   0x54410007
#define ATAG_CMDLINE    0x54410009

// structures for each tag

struct atag_header {
  bit32 size; // length of tag in words including this header
  bit32 tag ; // tag type
};

struct atag_core {
  bit32 flags;
  bit32 pagesize;
  bit32 rootdev;
};

struct atag_mem {
  bit32     size;
  bit32     start;
};

struct atag_ramdisk {
  bit32 flags;
  bit32 size;
  bit32 start;
};

struct atag_initrd2 {
  bit32 start;
  bit32 size;
};

struct atag_serialnr {
  bit32 low;
  bit32 high;
};

struct atag_revision {
  bit32 rev;
};

struct atag_cmdline {
  char cmdline[1];
};

struct atag {
  struct atag_header hdr;
  union {
    struct atag_core     core    ;
    struct atag_mem      mem     ;
    struct atag_ramdisk  ramdisk ;
    struct atag_initrd2  initrd2 ;
    struct atag_serialnr serialnr;
    struct atag_revision revision;
    struct atag_cmdline  cmdline ;
  } u;
};

#define tag_next(t)     ((struct atag *)((bit32 *)(t) + (t)->hdr.size))
#define tag_size(type)  ((sizeof(struct atag_header) + sizeof(struct type)) >> 2)

static struct atag * params; // pointer to current tag

static void zImageSetupCoreTag( void * address, long pagesize) {
  params = (struct atag *) address;
  params->hdr.tag = ATAG_CORE;
  params->hdr.size = tag_size( atag_core);
  params->u.core.flags = 1;
  params->u.core.pagesize = pagesize;
  params->u.core.rootdev = 0;
  params = tag_next( params);
}

static void zImageSetupMemTag( bit32 start, bit32 len) {
  params->hdr.tag = ATAG_MEM;
  params->hdr.size = tag_size( atag_mem);
  params->u.mem.start = start;
  params->u.mem.size = len;
  params = tag_next( params);
}

static void zImageSetupCmdlineTag( ccptr line) {
  bit32 linelen = StrLen( line);
  if (linelen) {
    params->hdr.tag = ATAG_CMDLINE;
    params->hdr.size = (sizeof( struct atag_header) + linelen + 1 + 4) >> 2;
    StrCpy( params->u.cmdline.cmdline, line);
    params = tag_next( params);
} }

static void zImageSetupEndTag( void) {
  params->hdr.tag = ATAG_NONE;
  params->hdr.size = 0;
}

//============================================================================
//  Check the integrity of the header segment of a linux image.  Return 1 on
//  success, zero on failure.  Verbosity levels can be e_Silent, e_Quiet or
//  e_Verbose.  Header is modified in-place and upon return is in native
//  byte ordering.
//============================================================================

bit32 zImageCheckHeader( p_zImageHeader Header, e_DisplayLevel DisplayLevel) {
  bit32 ImageSize = Header->End - Header->Start;
  if (DisplayLevel == e_Verbose) {
    TtyPutStrLn( "zImage:");
    TtyPutStr( "  Magic . . . $"); TtyPutHex( Header->Magic, 8);
  }
  if (Header->Magic == ZIMAGE_MAGIC) {
    if (DisplayLevel == e_Verbose) {
      TtyPutStrLn( " (ok)");
    }
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( "  Start . . . $"); TtyPutHex( Header->Start, 8); TtyPutLn();
      TtyPutStr( "  End . . . . $"); TtyPutHex( Header->End  , 8); TtyPutLn();
      TtyPutStr( "  Size  . . ."  ); TtyPutDec( ImageSize   , 10);
      if (ImageSize > SdramSize - BOARD_SDRAM_LINUX_OFF) {
        ImageSize = 0;
        TtyPutStrLn( " (bad, limit ");
        TtyPutDec( SdramSize - BOARD_SDRAM_LINUX_OFF, 1);
        TtyPutStrLn( ")");
      } else {
        TtyPutStrLn( " (ok)");
        return ImageSize; // success
    } }
    if (DisplayLevel == e_Quiet) {
      if (ImageSize > SdramSize - BOARD_SDRAM_LINUX_OFF) {
        ImageSize = 0;
        TtyPutStr( " (bad)");
      } else {
        TtyPutStr( " zImage (ok)");
    } }
  } else {
    ImageSize = 0;
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( " (bad, expected $");
      TtyPutHex( ZIMAGE_MAGIC, 8);
      TtyPutStrLn( ")");
    }
    if (DisplayLevel == e_Quiet) {
      TtyPutStr( " zImage (bad)");
  } }
  return ImageSize;
}

// Booting a uImage means first loading the uImage header, then the uImage data (which
// is a zImage), then constructing a tag list and finally jumping to the start of the
// executable zImage.

void uImageGoLinux( ccptr PartitionName, bit32 FlashAddress, bit32 FlashBlocks) {
  char Buffer[128];
  ccptr BootArgs = StrCpy( Buffer, GetEnvString( "bootargs"));
  bit32 PartitionBytes = FlashBlocks * JF_BLOCKSIZE;
  TtyPutStr( "\nBooting uImage from ");
  TtyPutStr( PartitionName);
  TtyPutStrLn( "...");
  if (JF_Init( JF_BLOCKSIZE)) {
    UsbLedColor( COLOR_LOAD);
    p_uImageHeader uHeader = (p_uImageHeader) ADDRESS(BOARD_SDRAM_BASE);
    if (JF_Read( FlashAddress, uImageHeaderSize, (bptr) uHeader) == FLASH_OK) {
      FlashAddress += uImageHeaderSize;
      if (uImageCheckHeader( uHeader, PartitionBytes, e_Verbose)) {
        bit32 uImageSize = uHeader->DataSize;
        p_zImageHeader zHeader = (p_zImageHeader) ADDRESS(BOARD_SDRAM_LINUX);
        if (JF_Read( FlashAddress, zImageHeaderSize, (bptr) zHeader) == FLASH_OK) {
          bit32 zImageSize = zImageCheckHeader( zHeader, e_Verbose);
          if (zImageSize) {
            bit32 kImageSize = uImageSize - zImageHeaderSize;
            FlashAddress += zImageHeaderSize;
            zImageSize   -= zImageHeaderSize;
            RestartWatchdog();
            CmBeep( 660, 4);
            if (JF_Read( FlashAddress, kImageSize, (bptr) BOARD_SDRAM_LINUX+zImageHeaderSize) == FLASH_OK) {
              CmBeep( 880, 4);
              TtyPutStr( "Starting Linux at $"); TtyPutHex( BOARD_SDRAM_LINUX, 8); TtyPutStrLn("...");
              UsbLedColor( COLOR_LINUX);
              zImageSetupCoreTag( (void *) (BOARD_SDRAM_BASE + 0x100), 4096);
              zImageSetupMemTag( BOARD_SDRAM_BASE, SdramSize);
              if (StrLen( BootArgs) == 0) {
                BootArgs = (SdramSize == MB(32)) ? "mem=32M console=/dev/ttyS0" : "mem=64M console=/dev/ttyS0";
              }
              zImageSetupCmdlineTag( BootArgs);
              zImageSetupEndTag();
              RestartWatchdog();
              if (SysType == e_OptiCell) {
                zImageGoLinuxOc();
              } else {
                zImageGoLinuxNb();
              }
            } else {
              TtyPutFailure( StrRead);
          } }
        } else {
          TtyPutFailure( StrRead);
    } } }
  } else {
    TtyPutFailure( StrInit);
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

