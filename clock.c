//-------------------------------------------------------------------------------------------
//  clock.c
//-------------------------------------------------------------------------------------------

#include "main.h"

#define iCacheEnabled  true

// Roughly 7 ARM core cycles per loop, which can be slowed down to MCLK cycles
// if iCache is disabled.  Loop delays are therefor:
//
//   iCachEnabled  ProcessorClock  MasterClock  Loop Time
//   ------------  --------------  -----------  ---------
//         1          96 MHz            -          68 nS
//         1         400 MHz            -          17 nS
//         0            -             48 MHz      187 nS
//         0            -            100 MHz       88 nS
//         0            -            133 MHz       66 nS

void DelayRaw( bit32 Count) {
  while (Count--) {
    asm volatile("nop");
} }

// Configure the delay constants needed by DelayRaw() based on the current
// clock and icache environment.  This routine needs to be called every
// time the clock or cache are reconfigured.

bit32 DelayRaw_15ns   = 0;
bit32 DelayRaw_66ns   = 0;
bit32 DelayRaw_400ns  = 0;
bit32 DelayRaw_50us   = 0;
bit32 DelayRaw_200us  = 0;
bit32 DelayRaw_1ms    = 0;

// Delay multipliers were empirically determined for 12 MHz crystal and MCLK
// configured for 48/100/133 MHz, with and without iCache enabled.

static void DelayConfig( void) {
  if (iCacheEnabled) {
    switch (ProcessorClock) {
      case MHZ( 96): DelayRaw_1ms = 14800; break;
      case MHZ(400): DelayRaw_1ms = 60000; break;
    }
  } else {
    switch (MasterClock) {
      case MHZ( 48): DelayRaw_1ms =  5356; break;
      case MHZ(100): DelayRaw_1ms = 11371; break;
      case MHZ(133): DelayRaw_1ms = 15161; break;
  } }
  DelayRaw_200us = (DelayRaw_1ms /     5) + 1;
  DelayRaw_50us  = (DelayRaw_1ms /    20) + 1;
  DelayRaw_400ns = (DelayRaw_1ms /  2500) + 1;
  DelayRaw_66ns  = (DelayRaw_1ms / 15152) + 1;
  DelayRaw_15ns  = (DelayRaw_1ms / 66667) + 1;
}

// Delay for the specified number of milliseconds.  The DelayConfig() routine
// above should already have been called.

void DelayMs( bit32 Milliseconds) {
  DelayRaw( Milliseconds * DelayRaw_1ms);
}

// Terminology:
//
//        Slow Clock = 32 KHz XTAL/RC oscillator
//        Main Clock = 12 MHz XTAL/RC oscillator
//      Master Clock = 48/133 Mhz (before and after ClockConfig)
//   Processor Clock = 96/400 Mhz (before and after ClockConfig)

// The Master Clock determines how much stress we put on SDRAM memory access.  Although it
// should run at 133 MHz we seem to have problems trying to push it that fast.  The Processor
// clock should be good to 400 MHz.

// To generate desired Master and Processor Clocks, we start by programming PLLA for a multiple
// of the 12 MHz Main Clock as follows:
//
//    PLLA = 12 MHz * MULA / PLLA_DIVA
//                     ||     ||
//                     ||    PLLA_DIVA can range from 1 to 255
//                     ||
//                    MULA = PLLA_MULA+1, and can range from 2 to 255
//
// There are other aspects such as charge-pump configuration which must also be set properly
// depending on the final frequency of PLLA.  We assume multiplier 200 and divisor 3 for an
// 800 MHz result.

// Next, we divide PLLA to get the Processor Clock as follows:
//
//    Processor Clock = PLLA / PLLADIV2 / PRES
//
// Since PLLADIV2 and PRES give a power-of-two divisor, processor clock can be 400, 200,
// 100 etc.  We then divide the Processor Clock by 2, 3, or 4 to get Master Clock.
//
//    Master Clock = Processor Clock / MDIV

bit32      SlowClock =    32768; // constant
bit32      MainClock = 12000000; // constant
bit32    MasterClock = 48000000; // 48/100/133 (RomBOOT starts us out at 48 MHz)
bit32 ProcessorClock = 96000000; // 96/400/400

bit8 FastClockSetFlag = 0; // 0=initial value, 1=explicitly set

#define FAST_PLLA_MULA  (199 << 16)
#define FAST_PLLA_COUNT ( 63 <<  8)

#define SLOW_PLLA_MULA  ( 63 << 16)
#define SLOW_PLLA_COUNT ( 17 <<  8)

// If fast clock not in desired mode, flip it to desired mode.  In fast mode the processor
// clock is increased to 400 MHz from its conservative 96 MHz value inherited from RomBOOT.

void FastClockConfig( void) {
  if (FastClockSetFlag == 0) {
    FastClockSetFlag = 1; // only initialize on 1st call
    MasterClock = MHZ(133);
  } else {
    switch (MasterClock) {
      case MHZ( 48): MasterClock = MHZ(100); break;
      case MHZ(100): MasterClock = MHZ(133); break;
      case MHZ(133): MasterClock = MHZ( 48); break;
  } }
  // Assume PLLA charge pump already set up for 800 MHz.  We presume this to be the case
  // as the datasheet specifies a reset value for this register of $01000100 which has
  // the ICPLLA bit set to 0 already and the register is write-only so we can't preserve
  // the other bits (which are total mysteries).
  switch (MasterClock) {
    case MHZ(48):
      // PLLA = MAIN=12MHz * PLLA_MULA=64 / PLLA_DIVA=1 = 768 MHz (ICPLLA=0, OUTA=0) 
      MemPut32( AT91C_PMC_PLLAR, AT91C_CKGR_SRCA | // 0x20000000  required
                                 SLOW_PLLA_MULA  | // 0x00c70000  multiply by 63+1
                                 SLOW_PLLA_COUNT | // 0x00001100  wait 17 slow clocks
                                 1              ); // 0x00000001  divide by 1
      ProcessorClock = 96000000; // 96 MHz
      break;
    case MHZ(100):
    case MHZ(133):
      // PLLA = MAIN=12MHz * PLLA_MULA=200 / PLLA_DIVA=3 = 800 MHz (ICPLLA=0, OUTA=0) 
      MemPut32( AT91C_PMC_PLLAR, AT91C_CKGR_SRCA | // 0x20000000  required
                                 FAST_PLLA_MULA  | // 0x00c70000  multiply by 199+1
                                 FAST_PLLA_COUNT | // 0x00003f00  wait 63 slow clocks
                                 3              ); // 0x00000003  divide by 1
      ProcessorClock = 400000000; // 400 MHz
      break;
  }
  while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_LOCKA ) == 0) ; // wait for PLLA locked
  while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_MCKRDY) == 0) ; // wait for MasterClock ready
  switch (MasterClock) {
    case MHZ(48):
      // Processor Clock = PLLA=768MHz / PRES=4 / PLLADIV2=2          = 96 MHz
      //    Master Clock = PLLA=768MHz / PRES=4 / PLLADIV2=2 / MDIV=2 = 48 MHz
      MemPut32( AT91C_PMC_MCKR, AT91C_PMC_PLLADIV2_2   | // 0x00001000  divide by 2
                                AT91C_PMC_MDIV_2       | // 0x00000100  divide by 2
                                AT91C_PMC_PRES_CLK_4   | // 0x00000000  divide by 4
                                AT91C_PMC_CSS_PLLA_CLK); // 0x00000002  select plla (768 MHz)
      break;
    case MHZ(100):
      // Processor Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2          = 400 MHz
      //    Master Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2 / MDIV=3 = 133 MHz
      MemPut32( AT91C_PMC_MCKR, AT91C_PMC_PLLADIV2_2   | // 0x00001000  divide by 2
                                AT91C_PMC_MDIV_4       | // 0x00000200  divide by 4
                                AT91C_PMC_PRES_CLK     | // 0x00000000  divide by 0
                                AT91C_PMC_CSS_PLLA_CLK); // 0x00000002  select plla (800 MHz)
      break;
    case MHZ(133):
      // Processor Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2          = 400 MHz
      //    Master Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2 / MDIV=3 = 133 MHz
      MemPut32( AT91C_PMC_MCKR, AT91C_PMC_PLLADIV2_2   | // 0x00001000  divide by 2
                                AT91C_PMC_MDIV_3       | // 0x00000300  divide by 3
                                AT91C_PMC_PRES_CLK     | // 0x00000000  divide by 0
                                AT91C_PMC_CSS_PLLA_CLK); // 0x00000002  select plla (800 MHz)
      break;
  }
  while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_MCKRDY) == 0) ;
  DelayConfig();
}

bit8 SlowClockExternal = 0; // 0=internal, 1=external
bit8 SlowClockSetFlag  = 0; // 0=initial value, 1=explicitly set

// Set SlowClockExternal to 0 if the internal rc oscillator is active, 1 if the external crystal
// oscillator is active.  This setting is battery-backed and hence non-volatile, so we have
// to query it early in the boot process to determine its actual state.

// If slow clock not in desired mode, flip it to desired mode.  We can switch the slow clock
// from the internal RC oscillator to the external crystal oscillator if it isn't already
// configured that way (that setting is battery-backed and hence non-volatile).

void SlowClockConfig( void) {
  bit8 SwitchToExternal = 0;
  bit8 SwitchToInternal = 0;
  if (SlowClockSetFlag == 0) {
    SlowClockSetFlag = 1; // only auto-switch to external on 1st call
    SlowClockExternal = (MemGet32( AT91C_SYS_SLCKSEL) & AT91C_SLCKSEL_OSC32EN) ? 1 : 0;
    if (SlowClockExternal == 0) {
      SwitchToExternal = 1;
    }
  } else {
    if (SlowClockExternal) {
      SwitchToInternal = 1; // external -> internal
    } else {
      SwitchToExternal = 1; // internal -> external
  } }
  if (SwitchToExternal || SwitchToInternal) {
    TtyPutStr( "\nSwitching to ");
    TtyPutStr( SwitchToExternal ? "external" : "internal");
    TtyPutStr( " 32 KHz RTC oscillator..."); DelayMs( 100);
    bit32 SLCKSEL = MemGet32( AT91C_SYS_SLCKSEL);
    if (SwitchToExternal) {
      SlowClockExternal = 1;
      // Severe lockup problem here!  Examine datasheet carefully and verify delays!
      SLCKSEL |=  AT91C_SLCKSEL_OSC32EN; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs( 1000);
      SLCKSEL |=  AT91C_SLCKSEL_OSCSEL ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs(    5);
      SLCKSEL &= ~AT91C_SLCKSEL_RCEN   ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL);
    } else {
      SlowClockExternal = 0;
      SLCKSEL |=  AT91C_SLCKSEL_RCEN   ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs( 1000);
      SLCKSEL &= ~AT91C_SLCKSEL_OSCSEL ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs(    5);
      SLCKSEL &= ~AT91C_SLCKSEL_OSC32EN; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL);
    } 
    TtyPutOk();
    DelayMs( 500); // it seems to take quite a few cycles for the baudrate generator to restabilize
} }

//  Display current clock settings.

void TtyPutClock( void) {
  TtyPutLn();
	TtyPutStr( "     "); TtyPutStrLn( BOARD_CPU_TYPE);
	TtyPutStr( "       XTAL "); TtyPutDec(      MainClock / 1000000, 0);
  TtyPutStr( " MHz, CPU "  ); TtyPutDec( ProcessorClock / 1000000, 0);
  TtyPutStr( " MHz, MCLK " ); TtyPutDec(    MasterClock / 1000000, 0);
  TtyPutStr( " MHz, SCLK 32 KHz ");
  if (SlowClockExternal) {
    TtyPutStr( "Ex");
  } else {
    TtyPutStr( "In");
  }
  TtyPutStr( "ternal, iCache ");
  if (iCacheEnabled) {
    TtyPutStrLn( "On");
  } else {
    TtyPutStrLn( "Off");  
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

