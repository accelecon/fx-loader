//--------------------------------------------------------------------------
//  cm.h
//--------------------------------------------------------------------------

extern void CmOpen( void);
extern void CmPutChar( bit8 Data);
extern void CmPutBuffer( cbptr Buffer, bit32 Length);
extern bit8 CmGetReady( void);
extern bool CmGetByte( bptr Temp);
extern bool CmGetVersion( void);
extern void CmTest( void);
extern void CmBeep( bit32 Hz, bit32 Ticks);

extern ccptr CmVersion;

//--------------------------------------------------------------------------
//  end
//--------------------------------------------------------------------------
