#-------------------------------------------------------------------------------------------
#  Makefile for fx-loader, the machine loader for Accelerated Concepts FX-Series boards.
#-------------------------------------------------------------------------------------------

all: fx-loader.bin

#-------------------------------------------------------------------------------------------
#  Specify cross compiler for stand-alone build.  When built under buildroot this is
#  ignored.  The setting below matches that used by the external toolchain for crosstool-ng
#  in the current NetBridge-FX build setup.  Also needed this in .bashrc (for example):
#
#    export PATH=$PATH:/home/michael/x-tools/arm-3.8-linux-gnueabi/bin
#
#-------------------------------------------------------------------------------------------

CROSS_COMPILE=arm-3.8-linux-gnueabi-

#-------------------------------------------------------------------------------------------
#  assemble the reset vector handler and division support
#-------------------------------------------------------------------------------------------

SOBJS := start.o _udivsi3.o _umodsi3.o
SHDRS := start.h at91sam9x25.h acfx101.h Makefile

AS=$(CROSS_COMPILE)gcc
ASFLAGS=-g -Os -Wall -mthumb-interwork -ffreestanding

%.o : %.S $(SHDRS)
	@echo $(AS) $(ASFLAGS) -c -o $@ $<
	@$(AS) $(ASFLAGS) -c -o $@ $<

#-------------------------------------------------------------------------------------------
#  compile everything else
#-------------------------------------------------------------------------------------------

COBJS := main.o util.o gpio.o spi.o flash.o images.o time.o clock.o led.o sdram.o ub-env.o cm.o lcd.o
CHDRS := main.h util.h gpio.h spi.h flash.h images.h time.h clock.h led.h sdram.h ub-env.h cm.h lcd.h $(SHDRS)

CC=$(CROSS_COMPILE)gcc
CPPFLAGS=-ffunction-sections -g -Os -Wall -mthumb -mthumb-interwork -ffreestanding

%.o : %.c $(CHDRS)
	@echo $(CC) $(CPPFLAGS) -c -o $@ $<
	@$(CC) $(CPPFLAGS) -c -o $@ $<

#-------------------------------------------------------------------------------------------
#  link it all together and strip symbols
#-------------------------------------------------------------------------------------------

OBJS := $(SOBJS) $(COBJS)

LD=$(CROSS_COMPILE)ld
LDFLAGS=-nostartfiles -Map=fx-loader.map --cref -T fx-loader.lds --gc-sections -Ttext 0x000000
OBJCOPY=$(CROSS_COMPILE)objcopy

fx-loader.bin: $(OBJS)
	@echo $(LD) $(LDFLAGS) -n -o fx-loader.elf $(OBJS)
	@$(LD) $(LDFLAGS) -n -o fx-loader.elf $(OBJS)
	@$(OBJCOPY) --strip-all fx-loader.elf -O binary $@
	@( loader_bytes=`stat -c%s fx-loader.bin`; \
		if [ "$$loader_bytes" -gt 24576 ] ; then \
			echo ; \
			echo "     ************************************************************"; \
			echo "     *** Bootloader too large to fit into SRAM (limit 24576)! ***"; \
			echo "     ************************************************************"; \
			echo ; \
			exit 2;\
		else \
			echo "============================================================"; \
			echo "  Bootloader fits into SRAM (limit 24576, actual $$loader_bytes)."; \
			echo "============================================================"; \
			cp fx-loader.bin ~; \
		fi )

#-------------------------------------------------------------------------------------------
#  discard derived files
#-------------------------------------------------------------------------------------------

clean:
	@rm -f *.o
	@rm -f *~
	@rm -f .*~
	@rm -f fx-loader.bin
	@rm -f fx-loader.elf
	@rm -f fx-loader.map

#-------------------------------------------------------------------------------------------
#  end
#-------------------------------------------------------------------------------------------

