//-------------------------------------------------------------------------------------------
//  sdram.c
//-------------------------------------------------------------------------------------------

// Configure the SDR SDRAM based on the timing parameters listed in the Micron datasheet,
// going for maximum performance (burst length 8, interleaved).

#include "main.h"

bit32 SdramSize;
ccptr SdramPart;

// Full memory test.  Return 1 on success.

bit8 SdramTestFull( bit32 MemorySize, bit8 Verbose) {
  bit32 i;
  if (Verbose) TtyPutStr( "\nSDRAM_Write..."); 
  for (i = 0; i < MemorySize; i += 4) {
    MemPut32( BOARD_SDRAM_BASE + i, i);
    if (Verbose && ((i % MB(1)) == 0)) {
      TtyPutChr( '#');
      DelayMs( 100);
  } }
  if (Verbose) {
    TtyPutStrLn( "...OK");
    DelayMs( 5000);
    TtyPutStr( "SDRAM_Check..."); 
  }
  for (i = 0; i < MemorySize; i += 4) {
    if (MemGet32( BOARD_SDRAM_BASE + i) != i) {
      if (Verbose) TtyPutStrLn( "...BAD");
      return 0; // failure
    }
    if (Verbose && ((i % MB(1)) == 0)) {
      TtyPutChr( '#');
      DelayMs( 100);
  } }
  if (Verbose) TtyPutStrLn( "...OK");
  return 1; // success
}

// A quick memory test that will hang the system on failure if the
// HangOnError flag is nonzero, otherwise will return 1 on success.

bit8 SdramTestQuick( bit8 HangOnError) {
  bit32 i;
  for (i = SdramSize; i >= MEGABYTE; i -= MEGABYTE) {
    MemPut32( BOARD_SDRAM_BASE + i - 800, i);
  }
  for (i = SdramSize; i >= MEGABYTE; i -= MEGABYTE) {
    if (MemGet32( BOARD_SDRAM_BASE + i - 800) != i) {
      if (HangOnError) {
        Hang( "SdramConfig-Error-1");
      }
      return 0; // failure
  } }
  if (SdramTestFull( 1024, 0) == 0) {
    if (HangOnError) {
      Hang( "SdramConfig-Error-2");
    }
    return 0; // failure
  }
  return 1; // success
}

//-------------------------------------------------------------------------------------------
//  configure the Micron MT48LC16M16A2 SDR SDRAM speed grade to 0x6A, 0x7E or 0x75:
//-------------------------------------------------------------------------------------------

//                                     100  133  143  167
//efine CONFIG_MT48LC16M16A2  0x75  // CL2  CL3
#define CONFIG_MT48LC16M16A2  0x7e  //      CL2  CL3
//efine CONFIG_MT48LC16M16A2  0x6a  //                CL3

//-------------------------------------------------------------------------------------------
//  assure the enumeration macros above are defined according to the rules
//-------------------------------------------------------------------------------------------

#if (CONFIG_MT48LC16M16A2 != 0x6A) && (CONFIG_MT48LC16M16A2 != 0x7E) && (CONFIG_MT48LC16M16A2 != 0x75)
  #error "*** ERROR: CONFIG_MT48LC16M16A2 must 0x6A, 0x7E or 0x75!"
#endif

// The MT48LC16M16A2 part is an SDR SDRAM organized as 4 meg x 16 bits x 4 banks.  It
// is available in 3 speed grades (see below).  

// SAM9 CR bit patterns

#define CR_NR     2                   // CR.NR   : 2 bits - number of rows (8192, 13 bits)
#define CR_NC_32  1                   // CR.NC   : 2 bits - number of columns (512, 9 bits)
#define CR_NC_64  2                   // CR.NC   : 2 bits - number of columns (1024 10 bits)
#define CR_NB     0                   // CR.NB   : 1 bit  - number of banks (4)
#define CR_DECOD  1                   // CR.DECOD: 1 bit  - decoding (interleaved)
#define CR_CAS    PREFER_CAS_LATENCY  // CR.CAS  : 3 bits - cas latency

#define RTR_ROWS  8192 // used to calculate RTR:COUNT

// Select preferred cas latency of 2 or 3:

#define PREFER_CAS_LATENCY  2
//#define PREFER_CAS_LATENCY  3

// The 0x6A part is specified at 167 MHz CL3.  This is the part installed on all the
// early prototypes.

#if (CONFIG_MT48LC16M16A2 == 0x6A)
  #define TRAS  42  // ns
  #define TRC   60  // ns
  #define TRCD  18  // ns
  #define TRFC  60  // ns
  #define TRP   18  // ns
  #define TRRD  12  // ns
  #define TWR1  12  // ns
  #define TWR2   6  // ns + 1 MCLK
  #define TXSR  67  // ns
  #if (PREFER_CAS_LATENCY == 2)
    #undef  PREFER_CAS_LATENCY
    #define PREFER_CAS_LATENCY  3
  #endif
#endif

// The 0x7E part is specified at 143 MHz CL3 and 133 MHz CL2.  This is the part
// installed on the final prototypes and the production units.

#if (CONFIG_MT48LC16M16A2 == 0x7E)
  #define TRAS  37  // ns
  #define TRC   60  // ns
  #define TRCD  15  // ns
  #define TRFC  66  // ns
  #define TRP   15  // ns
  #define TRRD  14  // ns
  #define TWR1  14  // ns
  #define TWR2   7  // ns + 1 MCLK
  #define TXSR  67  // ns
#endif

// The 0x75 part is specified at 133 MHz CL3 and 100 MHz CL2.  This part has not
// been used so far.

#if (CONFIG_MT48LC16M16A2 == 0x75)
  #define TRAS  44  // ns
  #define TRC   66  // ns
  #define TRCD  20  // ns
  #define TRFC  66  // ns
  #define TRP   20  // ns
  #define TRRD  15  // ns
  #define TWR1  15  // ns
  #define TWR2   8  // ns + 1 MCLK
  #define TXSR  75  // ns
#endif

#define TREF  64  // ms, used to calculate RTR:COUNT

#define T2PR_TRTP  3  // TPR2.TRTP: cycles read to precharge

// SDRAM MR bit patterns

#define MR_CAS  PREFER_CAS_LATENCY  // MR.CL: 3 bits - cas latency
#define MR_WB   0                   // MR.WB: 1 bit  - write burst mode (programmed burst length)
#define MR_BL   3                   // MR.BL: 3 bits - burst length 8
#define MR_BT   1                   // MR.BT: 1 bit  - burst type (interleaved)

// Mode register command codes:

#define NRM 0x00
#define NOP 0x01
#define ABP 0x02
#define LMR 0x03
#define ARF 0x04

// Export current settings for display:

bit8 SdramCasLatency =  // cas latency
#if (MR_CAS == 2)
  2;
#else
  #if (MR_CAS == 3)
    3;
  #else
    #error "*** ERROR: Invalid SDRAM cas latency!"
  #endif
#endif

ccptr SdramBurstMode =  // burst mode
#if (MR_WB == 1)
  "Single Location Access";
#else
  "Programmed Burst Length";
#endif

ccptr SdramBurstType =  // burst type
#if (MR_BT == 1)
  "Interleaved";
#else
  "Sequential";
#endif

bit8 SdramBurstLength =  // burst length
#if (MR_BL == 0)
  1;
#else
  #if (MR_BL == 1)
    2;
  #else
    #if (MR_BL == 2)
      4;
    #else
      #if (MR_BL == 3)
        8;
      #else
        #error "*** ERROR: Invalid SDRAM burst length!"
      #endif
    #endif
  #endif
#endif

// Return the number of MCLK cycles (at 133 MHz, 7.5 ns) which guarantee the
// specified minimum delay ns.

static bit32 Cycles0( bit32 ns10) {
  return (ns10 + 74) / 75;
}

static bit32 Cycles1( bit32 ns) {
  return Cycles0( ns * 10);
}

// Return the number of MCLK cycles (at 133 MHz, 7.5 ns) which guarantee the
// specified minimum delay ns1 and in 1 MCLK + ns2.

static bit32 Cycles2( bit32 ns1, bit32 ns2) {
  bit32 c1 = Cycles0(  ns1 * 10      );
  bit32 c2 = Cycles0( (ns2 * 10) + 75);
  if (c1 > c2) {
    return c1;
  }
  return c2;
}

// Configure SDRAM at the specified size and then do a trivial memory test to verify
// successful configuration.  Return 1 on success, 0 on failure.  For operation at full
// bus speed (133 MHz) the CAS latency setting is critical, and is a function of the
// exact model of the SDRAM chip installed on the board.  We are using a -7E speed
// grade which allows CAS latency 2.  If you program this wrong, the board will work
// at a reduced clock speed but fail at full speed.

static void SdramConfigSize( bit32 Size) {
  bit32 Reg32;

  MemPut32( AT91C_PMC_SCER, AT91C_PMC_DDR);
  MemPut32( AT91C_CCFG_EBICSA, MemGet32( AT91C_CCFG_EBICSA) | AT91C_EBI_CS1A_SDRAMC);

  MemPut32( AT91C_DDR2C_MDR , 0x00000010); // 16-bit SDR-SDRAM (default)
  
  Reg32 = MemGet32(AT91C_DDR2C_CR) & ~0x0050007f; // clear DECOD/NB/CAS/NR/NC
  if (Size == MB(32)) {
    MemPut32( AT91C_DDR2C_CR, Reg32 | (CR_DECOD << 22)   // 1 bit  - sequence or interleaved
                                    | (CR_NB    << 20)   // 1 bit  - number of banks
                                    | (CR_CAS   <<  4)   // 3 bits - cas latency
                                    | (CR_NR    <<  2)   // 2 bits - number of rows
                                    | (CR_NC_32 <<  0)); // 2 bits - number of columns
  } else {
    MemPut32( AT91C_DDR2C_CR, Reg32 | (CR_DECOD << 22)   // 1 bit  - sequence or interleaved
                                    | (CR_NB    << 20)   // 1 bit  - number of banks
                                    | (CR_CAS   <<  4)   // 3 bits - cas latency
                                    | (CR_NR    <<  2)   // 2 bits - number of rows
                                    | (CR_NC_64 <<  0)); // 2 bits - number of columns
  }

  Reg32 = MemGet32( AT91C_DDR2C_T0PR) & ~0x00ffffff; // clear TRRD/TRP/TRC/TWR/TRCD/TRAS
  MemPut32( AT91C_DDR2C_T0PR, Reg32 | (Cycles1( TRRD      ) << 20)   // 4 bits - active bank A to active bank B
                                    | (Cycles1( TRP       ) << 16)   // 4 bits - row precharge delay
                                    | (Cycles1( TRC       ) << 12)   // 4 bits - row cycle delay
                                    | (Cycles2( TWR1, TWR2) <<  8)   // 4 bits - write recovery delay
                                    | (Cycles1( TRCD      ) <<  4)   // 4 bits - row to column delay
                                    | (Cycles1( TRAS      ) <<  0)); // 4 bits - active to precharge delay
  
  Reg32 = MemGet32( AT91C_DDR2C_T1PR) & ~0x0000ff1f; // clear TXSNR(TXSR)/TRFC
  MemPut32( AT91C_DDR2C_T1PR, Reg32 | (Cycles1( TXSR) << 8)   // 8 bits - exit self refresh delay
                                    | (Cycles1( TRFC) << 0)); // 5 bits - row cycle delay
  
  Reg32 = MemGet32( AT91C_DDR2C_T2PR) & ~0x00007000; // clear TRTP
  MemPut32( AT91C_DDR2C_T2PR, Reg32 | (T2PR_TRTP << 12)); // 3 bits - read to precharge

  //  MasterClock   TREF   RTR_ROWS  MasterClock*(TREF/1000))/RTR_ROWS -> RTR
  //  ------------  -----  --------  ----------------------------------------
  //  133000000 Hz  64 ms      8192                                      1039
  //  100000000 Hz  64 ms      8192                                       781
  //   48000000 Hz  64 ms      8192                                       375

  MemPut32( AT91C_DDR2C_RTR, ((MasterClock*TREF)/1000)/RTR_ROWS); // Refresh Timer

  // Issue first NOP and wait 200 us = 13333 (6 cycles per iteration, core at 400 MHz)
  // Issue second NOP and wait 400 ns = 27
  // Issue All Banks Precharge and wait 15 ns (TRP) = 2
  // Issue Auto Refresh and wait 66 ns (TRFC) = 5
  // Issue Load Mode Register and wait 15 ns (TMRD) = 2
  // Issue Normal

  MemPut32( AT91C_DDR2C_MR, NOP); MemPut32( BOARD_SDRAM_BASE, 0); DelayRaw( DelayRaw_200us);
  MemPut32( AT91C_DDR2C_MR, NOP); MemPut32( BOARD_SDRAM_BASE, 0); DelayRaw( DelayRaw_400ns);
  MemPut32( AT91C_DDR2C_MR, ABP); MemPut32( BOARD_SDRAM_BASE, 0); DelayRaw( DelayRaw_15ns);
  
  MemPut32( AT91C_DDR2C_MR, ARF);
  for (Reg32 = 0; Reg32 < 8; Reg32++)  {
    MemPut32( BOARD_SDRAM_BASE, 0); DelayRaw( DelayRaw_15ns);
  }
  
  MemPut32( AT91C_DDR2C_MR, LMR);
  MemPut32( BOARD_SDRAM_BASE | (MR_WB  << 9)      // 1 bit  - write burst mode programmed-burst-length/single-location-access
                             | (MR_CAS << 4)      // 3 bits - cas latency
                             | (MR_BT  << 3)      // 1 bit  - burst type sequential/interleaved
                             | (MR_BL  << 0), 0); // 3 bits - burst length 1/2/4/8
  DelayRaw( DelayRaw_15ns);
  
  MemPut32( AT91C_DDR2C_MR, NRM); MemPut32( BOARD_SDRAM_BASE, 0);
  SdramSize = Size;
}

// Configure SDRAM chip.  NetBridge has 64MB, OptiCell has 32MB.

void SdramConfig( void) {
  if (SysType == e_NetBridge) {
    SdramConfigSize( MB(64));
    SdramPart = "  ISSI IS42S16320B-75E";
  } else {
    SdramConfigSize( MB(32));
    SdramPart = "Micron MT48LC16M16-7E ";
} }

// Assure the cas latency is valid

#if (PREFER_CAS_LATENCY != 2) && (PREFER_CAS_LATENCY != 3)
  #error "*** ERROR: PREFER_CAS_LATENCY must be 2 or 3!"
#endif

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

