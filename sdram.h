//-------------------------------------------------------------------------------------------
//  sdram.h
//-------------------------------------------------------------------------------------------

extern void SdramConfig( void);
extern bit8 SdramTestQuick( bit8 HangOnError);
extern bit8 SdramTestFull( bit32 MemorySize, bit8 Verbose);

extern  bit8 SdramCasLatency ;  // cas latency
extern ccptr SdramBurstMode  ;  // burst mode
extern ccptr SdramBurstType  ;  // burst type
extern  bit8 SdramBurstLength;  // burst length

extern bit32 SdramSize;
extern ccptr SdramPart;

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

