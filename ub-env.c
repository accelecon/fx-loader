//-------------------------------------------------------------------------------------------
//  ub-env.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// The u-boot environment is a 32-bit crc followed by a sequence of ASCIIZ key=value strings,
// terminated by one or more zero bytes to fill out a buffer equal in size to the flash
// partition less 4 bytes.

typedef struct {
  bit32 Crc;
  bit8  Data[4]; // actual number of bytes is partition size - 4 (allowing for crc)
} s_uBootEnv, * p_uBootEnv;

static p_uBootEnv uBootEnvBuffer = NULL;
static bit32 uBootEnvSize = 0; // number of Data[] bytes
static bit32 uBootEnvUsed = 0; // number of Data[] bytes actually in use

// The ethernet address is of the form 00:27:04:10:0x:yz (for OptiCell-FX
// series devices) where AFX-xyz is the unit serial number.  Only decimal
// digits are allowed.

static char Serial[] = "AFX-000"; // for OptiCell
//                     " 000000";    for NetBridge

// For NetBridge-FX the serial will be a 5-digit hex number greater
// than $01000.
ccptr uBootEnvSerialGet( ccptr EthAddr) {
  if (StrLen( EthAddr) == 17) {
    if (SysType == e_NetBridge) {
      Serial[0] = ' ';
      Serial[1] = EthAddr[9];
      Serial[2] = EthAddr[10];
      Serial[3] = EthAddr[12];
    }
    Serial[4] = EthAddr[13];
    Serial[5] = EthAddr[15];
    Serial[6] = EthAddr[16];
  } else {
    if (SysType == e_NetBridge) {
      Serial[0] = '0';
      Serial[1] = '0';
      Serial[2] = '0';
      Serial[3] = '1';
      Serial[4] = '0';
      Serial[5] = '0';
      Serial[6] = '0';
  } }
  return Serial;  
}

// Append a string to a buffer.

static cptr uBootEnvAppendStr( cptr Cursor, ccptr String) {
  bit32 Length = StrLen( String);
  StrCpy( Cursor, String);
  return Cursor+Length;
}

static cptr uBootEnvAppendLn( cptr Cursor) {
  *Cursor++ = 0x00;
  return Cursor;
}

static cptr uBootEnvAppendStrLn( cptr Cursor, ccptr String) {
  return uBootEnvAppendLn( uBootEnvAppendStr( Cursor, String));
}

// Recalculate the u-boot environment crc and write the updated environment
// to flash.  Return true on success, false on failure.

bool uBootEnvSave( bit32 FlashAddress, bit32 PartitionBytes) {
  TtyPutFlash( StrInit);
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) {
    TtyPutOk();
    uBootEnvBuffer->Crc = uImageCrc32( 0, uBootEnvBuffer->Data, uBootEnvSize);
    TtyPutFlash( StrErase);
    if (JF_Erase( FlashAddress, PartitionBytes) == FLASH_OK) {
      TtyPutOk();
      TtyPutFlash( StrClear);
      if (JF_Clear( FlashAddress, PartitionBytes) == FLASH_OK) {
        TtyPutOk();
        TtyPutFlash( StrWrite);
        if (JF_Write( FlashAddress, PartitionBytes) == FLASH_OK) {
          TtyPutOk();
          TtyPutFlash( StrVerify);
          if (JF_Verify( FlashAddress, PartitionBytes) == FLASH_OK) {
            TtyPutOk();
            return true;
  } } } } }
  TtyPutBad();
  return false;
}

// Reset the u-boot environment to default values.  Automatically called on check
// failure, but limited to once per boot.

void uBootEnvReset( bit32 FlashAddress, bit32 PartitionBytes, bool Force) {
  static bool First = true;
  if (First || Force) {
    First = false;
    TtyPutLn();
    TtyPutStr( "Reset ub-env...");
    TtyPutDec( PartitionBytes, 1);
    TtyPutStrLn( " bytes...");
    uBootEnvSize = PartitionBytes-4;
    uBootEnvBuffer = (p_uBootEnv) ADDRESS(BOARD_SDRAM_BASE);
    MemSet( (bptr) uBootEnvBuffer, 0xff, PartitionBytes); // was 0x00
    cptr Cursor = (cptr) uBootEnvBuffer->Data;
    Cursor = uBootEnvAppendStrLn( Cursor, "baudrate=115200");

    Cursor = uBootEnvAppendStr( Cursor, "ethaddr=00:27:04:");
    Cursor = uBootEnvAppendStrLn( Cursor, (SysType == e_OptiCell) ? "10:05:00"
                                                                  : "03:02:02");

    Cursor = uBootEnvAppendStrLn( Cursor, "bootpart=a");
    Cursor = uBootEnvAppendStr( Cursor, "bootargs=mem=");
    Cursor = uBootEnvAppendStr( Cursor, SdramSize == MB(32) ? "32" : "64");
    Cursor = uBootEnvAppendStr( Cursor, "M console=ttyS0,115200 root=/dev/mtdblock5 rootdelay=0 acfx=");
    Cursor = uBootEnvAppendStrLn( Cursor,  (SysType == e_OptiCell) ? "oc"
                                                                   : "nb");

    Cursor = uBootEnvAppendStrLn( Cursor, "bootcmd=bootm " JF_ADDR_BOOT0);
    Cursor = uBootEnvAppendStrLn( Cursor, "bootdelay=5");
    Cursor = uBootEnvAppendStrLn( Cursor, "stdin=serial");
    Cursor = uBootEnvAppendStrLn( Cursor, "stdout=serial");
    Cursor = uBootEnvAppendStrLn( Cursor, "stderr=serial");
    uBootEnvAppendLn( Cursor);         
  
    uBootEnvSave( FlashAddress, PartitionBytes);  
} }

// Adjust the u-boot environment to select booting from the specified partition.

void uBootFileSysSelect( ccptr Name, char BootPart, char Mtd, ccptr Address) {
  TtyPutStr( "Selecting ");
  TtyPutStr( Name);
  TtyPutStr( "...");
  bool Hit1 = false, Hit2 = false, Hit3 = false;
  bit32 EnvBytes = JF_BLOCKS_UBENV * JF_BLOCKSIZE;
  if (uBootEnvCheck( JF_ADDR_UBENV, EnvBytes, ADDRESS(BOARD_SDRAM_BASE), e_Silent)) {
    bit32 Offset = 0;
    while ((uBootEnvBuffer->Data[Offset]) && ((Hit1 == false) || (Hit2 == false) || (Hit3 == false))) {
      cptr KeyValue = (cptr) uBootEnvBuffer->Data+Offset;
      if ((Hit1 == false) && (Compare( (bptr) KeyValue, (bptr) "bootpart=", 9, NULL) == 0)) {
        if (StrLen( KeyValue) == 10) {
          if (KeyValue[9] && (KeyValue[9] != BootPart)) {
            KeyValue[9] = BootPart;
            Hit1 = true;
      } } }
      if ((Hit2 == false) && (Compare( (bptr) KeyValue, (bptr) "bootargs=", 9, NULL) == 0)) {
        bit32 Offset2 = 9;
        while (KeyValue[Offset2] && (Hit2 == false)) {
          if (Compare( (bptr) KeyValue+Offset2, (bptr) "root=/dev/mtdblock", 18, NULL) == 0) {
            if (KeyValue[Offset2+18] && (KeyValue[Offset2+18] != Mtd)) {
              KeyValue[Offset2+18] = Mtd;
              Hit2 = true;
          } }
          Offset2++;
      } }
      if ((Hit3 == false) && (Compare( (bptr) KeyValue, (bptr) "bootcmd=bootm ", 14, NULL) == 0)) {
        if (StrLen( KeyValue) == 22) {
          if (Compare( (bptr) KeyValue+14, (bptr) Address, 8, NULL) != 0) {
            StrCpy( KeyValue+14, Address);
            Hit3 = true;
      } } }
      Offset += StrLen( KeyValue) + 1;
    }
    if (Hit1 || Hit2 || Hit3) {
      if (uBootEnvSave( JF_ADDR_UBENV, EnvBytes)) {
        TtyPutStrLn( "OK");
      } else {
        TtyPutStrLn( "ERROR saving environment!");
      }
    } else {
      TtyPutStrLn( "OK (no change)");
    }
  } else {
    TtyPutStrLn( "ERROR - Bad environment!");
} }

// Save an updated serial number to the u-boot (and optionally loader) environments by
// updating the last three nibbles of the ethaddr value.

static bool uBootEnvSerialSave( bool Loader) {
  bit32 EnvBytes = JF_BLOCKS_UBENV * JF_BLOCKSIZE;
  if (uBootEnvCheck( JF_ADDR_UBENV, EnvBytes, ADDRESS(BOARD_SDRAM_BASE), e_Silent)) {
    bit32 Offset = 0;
    while (uBootEnvBuffer->Data[Offset]) {
      cptr KeyValue = (cptr) uBootEnvBuffer->Data+Offset;
      if (Compare( (bptr) KeyValue, (bptr) "ethaddr=", 8, NULL) == 0) {
        if (StrLen( KeyValue) == 25) {
          if (SysType == e_NetBridge) {
            KeyValue[17] = Serial[1]; // 'ethaddr=00:27:04:uv:wx:yz' where vw is 01+
            KeyValue[18] = Serial[2]; //  0123456789012345678901234
            KeyValue[20] = Serial[3]; 
            KeyValue[21] = Serial[4];
            KeyValue[23] = Serial[5];
            KeyValue[24] = Serial[6];
          }
          if (SysType == e_OptiCell) {
            KeyValue[21] = Serial[4]; // 'ethaddr=00:27:04:10:0x:yz'
            KeyValue[23] = Serial[5]; //  0123456789012345678901234
            KeyValue[24] = Serial[6];
          }
          bool     Success = uBootEnvSave( JF_ADDR_UBENV                                                , EnvBytes);
          if (Loader) {
            return Success & uBootEnvSave( JF_ADDR_LOADER + (JF_BLOCKS_LOADER * JF_BLOCKSIZE) - EnvBytes, EnvBytes);
          }
          return Success;
      } }
      Offset += StrLen( KeyValue) + 1;
  } }
  return false;
}

// The lcd is initialized with the 3-digit serial number offset 4 characters
// into the unit number displayed starting in the specified column.  Position
// the cursor over the last digit and allow editing of the displayed value
// with the arrow keys.  Enter saves the new value and reboots.  Cancel aborts
// editing and returns.  Serial number must be 500 or greater.

void uBootEnvSerialEditLcd( void) {
  bit8 Cursor = 6;
  LcdPosCursor( LcdRowSerial, LcdColSerial+Cursor);
  LcdSetCursor( true);
  while (true) {
    switch (LcdKeyGet()) {
      case Key_Error:
      case Key_None:
        break;
      case Key_Up:
        if (Serial[Cursor] == '9') Serial[Cursor] = '0'; else Serial[Cursor]++;
        LcdPutRowCol( LcdRowSerial, LcdColSerial+4, Serial+4);
        LcdPosCursor( LcdRowSerial, LcdColSerial+Cursor);
        break;
      case Key_Down:
        if (Serial[Cursor] == '0') Serial[Cursor] = '9'; else Serial[Cursor]--;
        LcdPutRowCol( LcdRowSerial, LcdColSerial+4, Serial+4);
        LcdPosCursor( LcdRowSerial, LcdColSerial+Cursor);
        break;
      case Key_Left:
        if (Cursor > 4) Cursor--; else Cursor = 6;
        LcdPosCursor( LcdRowSerial, LcdColSerial+Cursor);
        break;
      case Key_Right:
        if (Cursor < 6) Cursor++; else Cursor = 4;
        LcdPosCursor( LcdRowSerial, LcdColSerial+Cursor);
        break;
      case Key_Enter:
        if ((Serial[4] == '5') || // Enforce 'AFX-500' or greater
            (Serial[4] == '6') ||
            (Serial[4] == '7') ||
            (Serial[4] == '8') ||
            (Serial[4] == '9')) {
          LcdSetCursor( false);
          uBootEnvSerialSave( true);
          return;
        }
        break;
      case Key_Cancel:
        LcdSetCursor( false); // parent logic will restore initial display
        return;
} } }

// Display the current serial number and allow it to be edited using backspace,
// lowercase hex digits and space.  Enter commits changes, Escape aborts them.

#define BackSpace  0x08
#define Enter      0x0d
#define Escape     0x1b
#define Del        0x7f

bool uBootEnvSerialEditTty( void) {
  bit8 c, Cursor, CursorLimit;
  TtyPutLn();
  if (SysType == e_OptiCell) {
    TtyPutStrLn( "Valid range AFX-500..AFX-999 decimal");
    CursorLimit = 4;
  } else {
    TtyPutStrLn( "Valid range 01000..fffff hex");
    CursorLimit = 1;
  }
  TtyPutStr( "Serial: ");
  TtyPutStr( Serial);
  Cursor = 4;
  TtyPutChr( BackSpace);
  TtyPutChr( BackSpace);
  TtyPutChr( BackSpace);
  while ((c = TtyGetChar())) {
    if (((SysType == e_NetBridge) && ((c >= 'a') && (c <= 'f'))) ||
                                     ((c >= '0') && (c <= '9') )) { 
      Serial[Cursor] = c;
      TtyPutChr( c);
      if (Cursor < 6) {
        Cursor++;
      } else {
        TtyPutChr( BackSpace);
      }
    } else {
      if ((c == BackSpace) || (c == Del)) {
        if (Cursor > CursorLimit) {
          TtyPutChr( BackSpace);
          Cursor--;
        }
      } else {
        if (c == ' ') {
          if (Cursor < 6) {
            TtyPutChr( Serial[Cursor++]);
          }
        } else {
          if (c == Enter) {
            if (SysType == e_OptiCell) {
              if ((Serial[4] == '5') || // Enforce 'AFX-500' or greater
                  (Serial[4] == '6') ||
                  (Serial[4] == '7') ||
                  (Serial[4] == '8') ||
                  (Serial[4] == '9')) {
                TtyPutLn();
                return uBootEnvSerialSave( true);
            } }
            if (SysType == e_NetBridge) {
              if ((Serial[2] != '0') || // Enforce '0001000' or greater
                  (Serial[3] != '0')) {
                TtyPutLn();
                return uBootEnvSerialSave( true);
            } }
          } else {
            if (c == Escape) {
              break;
  } } } } } }
  return false;
}

// Check the integrity of a u-boot environment.  Upon success, return true and set the global
// buffer and length values.  Return false on failure.

bool uBootEnvCheck( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, e_DisplayLevel DisplayLevel) {
  bit8 uBootEnvValid = 0;
  uBootEnvSize = PartitionBytes;
  uBootEnvBuffer = (p_uBootEnv) ReadBuffer;
  if (JF_Read( FlashAddress, uBootEnvSize, ReadBuffer) == FLASH_OK) {
    uBootEnvSize -= 4; // skip crc
    if (DisplayLevel == e_Verbose) {
      TtyPutStr( "  Data CRC . . . . . $"); TtyPutHex( uBootEnvBuffer->Crc, 8);
    }
    bit32 CheckCrc = uImageCrc32( 0, uBootEnvBuffer->Data, uBootEnvSize);
    if (CheckCrc == uBootEnvBuffer->Crc) {
      uBootEnvValid = 1;
      if (DisplayLevel != e_Silent) {
        for (uBootEnvUsed = uBootEnvSize-1; uBootEnvUsed > 10; uBootEnvUsed--) {
          if (uBootEnvBuffer->Data[uBootEnvUsed] == 0x00) {
            break;
        } }
        TtyPutStr( " (ok, ");
        TtyPutDec( (uBootEnvUsed * 100) / uBootEnvSize, 1);
        TtyPutStr( "%)");
        if (DisplayLevel == e_Verbose) {
          TtyPutLn();
      } }
    } else {
      if (DisplayLevel == e_Verbose) {
        TtyPutStr( " (bad, calculated $");
        TtyPutHex( CheckCrc, 8);
        TtyPutStrLn( ")");
  } } }
  if (uBootEnvValid) {
    return true; // success
  }
  if (DisplayLevel == e_Quiet) {
    TtyPutStr( " (bad)");
  }
  uBootEnvReset( FlashAddress, PartitionBytes, false);
  uBootEnvSize = 0;
  uBootEnvBuffer = NULL;
  return false; // failure
}

// Dump a list of all u-boot environment key=value pairs.

bool uBootEnvList( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer) {
  if (uBootEnvCheck( FlashAddress, PartitionBytes, ADDRESS(BOARD_SDRAM_BASE), e_Silent)) {
    bit32 Offset = 0;
    while (uBootEnvBuffer->Data[Offset]) {
      ccptr KeyValue = (ccptr) uBootEnvBuffer->Data+Offset;
      TtyPutStr( "  ");
      TtyPutStrLn( KeyValue);
      Offset += StrLen( KeyValue) + 1;
    }
    return true;
  }
  TtyPutStrLn( "*** Bad u-boot environment!");
  return false;
}

// Get an environment value corresponding to the specified key.  Return "" on failure.

ccptr uBootEnvStr( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, ccptr Key) {
  if (Key) {
    if (uBootEnvCheck( FlashAddress, PartitionBytes, ADDRESS(BOARD_SDRAM_BASE), e_Silent)) {
      bit32 Offset = 0, KeyLen = StrLen( Key);
      while (uBootEnvBuffer->Data[Offset]) {
        bptr KeyValue = uBootEnvBuffer->Data+Offset;
        if (Compare( (bptr) Key, KeyValue, KeyLen, NULL) == 0) {
          if (KeyValue[KeyLen] == '=') {
            return (ccptr) KeyValue+KeyLen+1;
        } }
        Offset += StrLen( (cptr) KeyValue) + 1;
  } } }
  return "";
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

