//-------------------------------------------------------------------------------------------
//  util.h
//-------------------------------------------------------------------------------------------

static void inline MemPut32( bit32 Address, bit32 Data) {
  volatile bit32 * Memory = (bit32 *) Address;
  *Memory = Data;
}

static bit32 inline MemGet32( bit32 Address) {
  volatile bit32 * Memory = (bit32 *) Address;
  return *Memory;
}

extern const cptr StrInit  ;
extern const cptr StrRead  ;
extern const cptr StrWrite ;
extern const cptr StrVerify;
extern const cptr StrClear ;
extern const cptr StrErase ;

void Hang( ccptr Message);
void Reset( void);

extern bit8  AllFoxes( cbptr Buffer                , bit32 ByteCount, bit32 * Offset);
extern bit8  Compare ( cbptr Buffer1, cbptr Buffer2, bit32 ByteCount, bit32 * Offset);
extern void  MemSet  (  bptr Target ,  bit8 Source , bit32 ByteCount);
extern void  MemCpy  (  bptr Target , cbptr Source , bit32 ByteCount);
extern bit32 StrLen  ( ccptr String);
extern  cptr StrCpy  (  cptr Target , ccptr Source);
extern bit8  StrCmp  ( ccptr Target , ccptr Source);
extern bit32 HexValue( ccptr String);

extern void  Dump( bit32 FlashAddress, cbptr Buffer, bit32 ByteCount);

typedef enum {
  e_None  ,
  e_Loader,
  e_lEnv  ,
  e_uBoot ,
  e_uEnv  ,
  e_uImage
} e_ImageType;

extern void DisplayMenuItem( char MenuSelection, char Partition, cptr BinName, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
extern void XmodemToAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
extern void XmodemToJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
extern void DumpAtmelFlash( void);
extern void DumpJedecFlash( void);
extern void ZapAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
extern void ZapJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
extern void TestAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
extern void TestJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
extern void CheckAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
extern void CheckJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);

extern void DisableWatchdog( void);
extern void RestartWatchdog( void);

extern bit8 TtyConfig( bit8 Fast, bit8 Announce);
extern bit8 TtyGetReady( void);
extern bit8 TtyGetChar( void);
extern void TtyPutChr( char c);
extern void TtyPutStr( ccptr s);
extern void TtyPutLn( void);
extern void TtyPutOk( void);
extern void TtyPutBad( void);
extern void TtyPutStrLn( ccptr s);
extern void TtyPutDec( bit32 Value, bit8 Digits);
extern void TtyPutDecPad( bit32 Value, bit8 Digits, char Pad);
extern void TtyPutHex( bit32 Value, bit8 Digits);
extern void TtyPutFailure( cptr Routine);
extern void TtyPutFlash( cptr Routine);
extern void TtyPutFlashGeometry( bit32 Blocks, bit32 BlockSize);

extern bit32 AtmelFlash_Offset;
extern bit32 JedecFlash_Offset;

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

