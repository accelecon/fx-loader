//--------------------------------------------------------------------------
//  lcd.c
//--------------------------------------------------------------------------

// The lcd module is a Crystalfontz CFA635-TMF-KL which has 4 lines of 20
// characters, 4 bi-color mixable leds, a 6-key keypad, and a 5v logic-level
// serial interface at 115200n81.  It uses a crc-checked frame-oriented
// interface protocol.

#include "main.h"

ccptr LcdVersion = NULL;

//--------------------------------------------------------------------------
// Define a signature of from 1 to 16 bytes in length that will be stored
// in the user flash area of the lcd display to verify that it has been
// initialized.  To force reinitialization, simply change the signature.
//--------------------------------------------------------------------------

#define LcdSigLength  8
static bit8 LcdSignature[] = "OPTICELL";

bit8 LcdColSerial, LcdColLast;
bit8 LcdRowSerial, LcdRowLast;

//--------------------------------------------------------------------------
// Private routines open, close and do crc-checked i/o with the serial port
// of the LCD display unit.
//--------------------------------------------------------------------------

static void LcdOpen( void) {
  bit32 BaudRate = 115200;
  bit32 BaudRateDivisor = MasterClock       / (BaudRate * 16)      ;
  bit32 BaudRateError = ((MasterClock * 10) / (BaudRate * 16)) % 10;
  if (BaudRateError > 4) {
    BaudRateDivisor += 1;
  }
  MemPut32( AT91C_PIOC_PDR, AT91C_PC8_UTXD0 | AT91C_PC9_URXD0);
  MemPut32( AT91C_UART0_IDR, ~0);
  MemPut32( AT91C_UART0_CR, AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS);
  MemPut32( AT91C_UART0_BRGR, BaudRateDivisor);
  MemPut32( AT91C_UART0_MR, AT91C_US_PAR_NONE);
  MemPut32( AT91C_UART0_CR, AT91C_US_RXEN | AT91C_US_TXEN);
}

static void LcdPutChar( bit8 Data) {
  while ((MemGet32( AT91C_UART0_CSR) & AT91C_US_TXRDY) == 0) ;
  MemPut32( AT91C_UART0_THR, Data);
}

static void LcdPutBuffer( cbptr Buffer, bit32 Length) {
  while (Length--) {
    LcdPutChar( *Buffer++);
} }

static bit8 LcdGetReady( void) {
  return (MemGet32( AT91C_UART0_CSR) & AT91C_US_RXRDY) ? 1 : 0;
}

// Wait for up to DelayMs for a receive byte, checking every 50us.

static bool LcdGetByte( bptr Data) {
  if (LcdGetReady()) {
    *Data = MemGet32( AT91C_UART0_RHR) & 0xff;
    return true;
  }
  DelayRaw( DelayRaw_50us);
  return false;
}

static bit16 Crc16( bptr bufptr, bit16 len) {
  bit32 newCRC;
  bit8 data;
  int32 bit_count;
  newCRC = 0x00F32100;
  while (len--) {
    data = *bufptr++;
    for (bit_count = 0; bit_count <= 7; bit_count++) {
      newCRC >>= 1;
      if (data & 0x01) {
        newCRC |= 0x00800000;
      }
      if (newCRC & 0x00000080) {
        newCRC ^= 0x00840800;
      }
      data >>= 1;
  } }
  for (bit_count = 0; bit_count <= 15; bit_count++) {
    newCRC >>= 1;
    if (newCRC & 0x00000080) {
      newCRC ^= 0x00840800;
  } }
  return (~newCRC) >> 8;
}

#define LCD_MAX  80

static bit8 LcdCmd[LCD_MAX];
static bptr LcdRsp = NULL;

static void PacketXmit( void) {
  int Length = LcdCmd[1];
  bit16 CheckSum = Crc16( LcdCmd, Length+2);
  LcdCmd[Length+2] = CheckSum & 255;
  LcdCmd[Length+3] = CheckSum >> 8;
  LcdPutBuffer( LcdCmd, Length+4);
  RestartWatchdog();
}

// The response packet format is:
//
//   (CMD|0x40) LEN xx xx .. xx  CRC1    CRC2
//    [0]       [1]             [LEN+2] [LEN+3]

static bit8 LcdRcvData[LCD_MAX];
static bit8 LcdRcvCount = 0;

static bool PacketRecv( bit8 Rsp) {
  bit8 Temp;
  bit32 Loops;
  LcdRsp = NULL;
  LcdRcvCount = 0;
  MemSet( LcdRcvData, 0, LCD_MAX);
  for (Loops = 0; Loops < 5000; Loops++) { // wait up to 250 ms
    if (LcdGetByte( &Temp)) {             //   for 1st response byte
      LcdRcvData[LcdRcvCount++] = Temp;
      for (Loops = 0; Loops < 1000; Loops++) { // while idle time less than 50 ms
        if (LcdGetByte( &Temp)) {            //   collect response bytes
          if (LcdRcvCount < LCD_MAX) {
            LcdRcvData[LcdRcvCount++] = Temp;
            Loops = 0;
      } } }
      break;
  } }
  if ((LcdRcvCount > 3) && (LcdRcvData[0] == Rsp)) {
    bit8 Length = LcdRcvData[1];
    bit16 Crc = LcdRcvData[ Length+2];
    Crc |= (LcdRcvData[ Length+3] << 8);
    if (Crc == Crc16( LcdRcvData, Length+2)) {
      LcdRsp = LcdRcvData;
  } }
  return LcdRsp ? true : false;
}

// Send a packet and parse the reply.

static bool SendRecv( int Retry) {
  PacketXmit();
  if (PacketRecv( 0x40 | LcdCmd[0])) {
    return true;
  } else {
    if (Retry) {
      return SendRecv( Retry-1);
  } }
  return false;
}

// Set cursor position.

bool LcdPosCursor( bit8 Row, bit8 Col) {
  LcdCmd[0] = 11; // 0x0B Set LCD Cursor Position
  LcdCmd[1] = 2;
  LcdCmd[2] = Col;
  LcdCmd[3] = Row;
  return SendRecv( 2);
}

// Set cursor to underscore or none.

bool LcdSetCursor( bool Flag) {
  LcdCmd[0] = 12; // 0x0C Set LCD Cursor Style
  LcdCmd[1] = 1;
  LcdCmd[2] = Flag ? 2 : 0;
  return SendRecv( 2);
}

// Set backlight percent.

static bool SetBackLight( bit8 Percent) {
  LcdCmd[0] = 14; // 0x0E Set LCD & Keypad Backlight
  LcdCmd[1] = 1;
  LcdCmd[2] = Percent;
  return SendRecv( 2);
}

// Control signals reset to factory defaults.

static bool SetDefaults( void) {
  LcdCmd[0] = 28; // 0x1C Set ATX Power Switch Functionality
  LcdCmd[1] = 1;
  LcdCmd[2] = 0;
  return SendRecv( 2);
}

// No keystroke notification (will poll).

static bool SetKeyPoll( void) {
  LcdCmd[0] = 23; // 0x17 Configure Key Reporting
  LcdCmd[1] = 2;
  LcdCmd[2] = 0;
  LcdCmd[3] = 0;
  return SendRecv( 2);
}

// Record boot state.

static bool LcdSetBoot( void) {
  LcdCmd[0] = 4; // 0x04 Store Current State As Boot State
  LcdCmd[1] = 0;
  return SendRecv( 2);
}

// Clear the lcd display.

static void LcdClrScreen( void) {
  LcdCmd[0] = 6; // 0x06 Clear LCD Screen
  LcdCmd[1] = 0;
  SendRecv( 2);
}

// Get the lcd version.

static void LcdGetVersion( void) {
  static bit8 Buffer[18];
  LcdCmd[0] = 1; // 0x01 Get Hardware & Firmware Version
  LcdCmd[1] = 0;
  if (SendRecv( 2)) {
    if (LcdRsp[1] == 16) {
      MemCpy( Buffer, LcdRsp+2, 16);
      Buffer[16] = 0;
      LcdVersion = (ccptr) Buffer;
} } }

// Clear any pending keystrokes from the lcd display keyboard.

static void LcdKeyClr( void) {
  LcdCmd[0] = 24; // 0x18 Read Keypad, Polled Mode
  LcdCmd[1] = 0;
  SendRecv( 2);
}

// Set the 'initialized' flag in the lcd flash.

static bool LcdSetFlag( void) {
  LcdCmd[0] = 2; // 0x02 Write User Flash Area
  LcdCmd[1] = 16;
  MemSet( LcdCmd+2, 0, 16);
  MemCpy( LcdCmd+2, LcdSignature, LcdSigLength);
  return SendRecv( 2);
}

// Query the state of the 'initialized' flag in the lcd flash.

static bool LcdGetFlag( void) {
  LcdCmd[0] = 3; // 0x03 Read User Flash Area
  LcdCmd[1] = 0;
  if (SendRecv( 2)) {
    if (Compare( LcdRsp+2, LcdSignature, LcdSigLength, NULL) == 0) {
      return true;
  } }
  return false;
}

// There are 4 red/green bicolor leds, 0 (bottom) to 3 (top).  Each may be set to any
// mixture of red/green by percentage 0..100.

static bool LcdLedSetRedGrn( bit8 Led, bit8 Red, bit8 Grn) {
  bool rc = false;
  Led *= 2;
  Led += 5;
  LcdCmd[0] = 34; // 0x22 Set or Set and Configure GPIO Pin
  LcdCmd[1] = 2;
  LcdCmd[2] = Led;
  LcdCmd[3] = Grn;
  if (SendRecv( 2)) {
    Led += 1;
    LcdCmd[0] = 34; // 0x22 Set or Set and Configure GPIO Pin
    LcdCmd[1] = 2;
    LcdCmd[2] = Led;
    LcdCmd[3] = Red;
    rc = SendRecv( 2);
  }
  return rc;
}

static void LcdLedSetAll( bit8 Color) {
  LcdLedSet( 0, Color);
  LcdLedSet( 1, Color);
  LcdLedSet( 2, Color);
  LcdLedSet( 3, Color);
}

//--------------------------------------------------------------------------
// Public routines format commands to the LCD display unit and utilize the
// private routines above to implement them.
//--------------------------------------------------------------------------

// Wait up to 6 seconds for the lcd module to become responsive to commands.
// It seems to take around 4 seconds from my limited observations.

bit32 LcdInitMs = 0;

void LcdInit( void) {
  bit8 Loops = 0;
  TtyPutStr( "LCD init...");
  LcdOpen();
  DelayMs( 50);
  while ((Loops++ < 30) && !SetDefaults()) {
    LcdInitMs += 200;
    DelayMs( 200);
  }
  if (SetBackLight( 30)) {
    LcdSetCursor( false);
    LcdLedSetAll( Color_Blk);
    SetKeyPoll();
    LcdClrScreen();
    if (LcdGetFlag()) {
      TtyPutStrLn( "normal");
    } else {
      LcdSetFlag();
      LcdSetBoot();
      TtyPutStrLn( "reflash");
    }
    LcdGetVersion();
    LcdKeyClr();
  } else {
    TtyPutStrLn( "failure!");
} }

void LcdPutRowCol( bit8 Row, bit8 Col, ccptr Text) {
  if (Text) {
    bit8 Length = StrLen( Text);
    LcdCmd[0] = 31; // 0x1f Send Data to LCD
    LcdCmd[1] = Length+2;
    LcdCmd[2] = Col;
    LcdCmd[3] = Row;
    for (Col = 0; Col < Length; Col++) {
      LcdCmd[Col+4] = Text[Col];
    }
    SendRecv( 2);
} }

void LcdPutRow( bit8 Row, ccptr Text) {
  LcdColLast = (20 - StrLen( Text)) / 2;
  LcdPutRowCol( LcdRowLast = Row, LcdColLast, Text);
}

static bit8 LcdRow;
static bit8 LcdCol;

void LcdPutCharNext( char Value) {
  LcdCmd[0] = 31; // 0x1f Send Data to LCD
  LcdCmd[1] = 3;
  LcdCmd[2] = LcdCol++;
  LcdCmd[3] = LcdRow;
  LcdCmd[4] = Value;
  SendRecv( 2);
}

static void LcdPutDec( bit32 Value) {
  bit8 Digit = Value % 10;
       Value = Value / 10;
  if (Value) {
    LcdPutDec( Value);
  }
  LcdPutCharNext( Digit + '0');
}

void LcdPutRowColDec( bit8 Row, bit8 Col, bit32 Value) {
  LcdRow = Row;
  LcdCol = Col;
  LcdPutDec( Value);
}

// There are 4 red/green bicolor leds, 0 (bottom) to 3 (top).  Each may be set to any
// mixture of red/green by percentage 0..100.

void LcdLedSet( bit8 Led, bit8 Color) {
  bit8 Red = 0, Grn = 0;
  switch (Color) {
    case Color_Red:            Red = 60; break;
    case Color_Yel: Grn =  20; Red = 60; break;
    case Color_Grn: Grn =  60;           break;
  }
  LcdLedSetRedGrn( Led, Red, Grn);
}

e_LcdKey LcdKeyGet( void) {
  e_LcdKey Key = Key_Error;
  LcdCmd[0] = 24; // 0x18 Read Keypad, Polled Mode
  LcdCmd[1] = 0;
  if (SendRecv( 2)) {
    Key = Key_None;
    if (LcdRsp[0] == 0x58) { // 0x18 | 0x40
      if (LcdRsp[1] == 3) { // length as expected
        bit8 KeysNew = LcdRsp[3]; // keys  pressed since last poll
        if (KeysNew) { // keys pressed since last poll
          if (KeysNew & Key_Cancel) Key = Key_Cancel; else
          if (KeysNew & Key_Enter ) Key = Key_Enter ; else
          if (KeysNew & Key_Up    ) Key = Key_Up    ; else
          if (KeysNew & Key_Down  ) Key = Key_Down  ; else
          if (KeysNew & Key_Left  ) Key = Key_Left  ; else
          if (KeysNew & Key_Right ) Key = Key_Right ;
          if (Key != Key_None) {
            LcdKeyClr();
  } } } } }
  return Key;
}

// If the lcd is responsive, each look will take 50 ms.  If the lcd is totally
// unresponsive, each loop will take 500 ms (two retries of 250 ms each).

void LcdTest( void) {
  TtyPutStr( "Waiting 10 seconds for LCD key...");
  bit32 ms = 0;
  while (ms < 10000) {
    ms += 50;
    if (ms % 1000 == 0) {
      TtyPutStr( "...");
    }
    e_LcdKey Key = LcdKeyGet(); // 50 ms delay
    switch (Key) {
      case Key_None  :                          break;
      case Key_Cancel: TtyPutStr( "Cancel..."); break;
      case Key_Enter : TtyPutStr( "Enter..." ); break;
      case Key_Up    : TtyPutStr( "Up..."    ); break;
      case Key_Down  : TtyPutStr( "Down..."  ); break;
      case Key_Left  : TtyPutStr( "Left..."  ); break;
      case Key_Right : TtyPutStr( "Right..." ); break;
      case Key_Error : TtyPutStrLn( "Error!"); return;
  } }
  TtyPutLn();
}

//--------------------------------------------------------------------------
//  end
//--------------------------------------------------------------------------
