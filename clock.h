//-------------------------------------------------------------------------------------------
//  clock.h
//-------------------------------------------------------------------------------------------

// slow clock control

extern void SlowClockConfig( void); // cycle internal/external 32 KHz

// fast clock control

extern void FastClockConfig( void); // cycle 48/100/133 MHz

// display clock settings

extern void TtyPutClock( void);

// current clock settings

extern bit32      SlowClock; // 32 KHz RC/XTAL
extern bit32      MainClock; // 12 MHz RC/XTAL
extern bit32    MasterClock; // 48/100/133 MHz before/after ClockConfig()
extern bit32 ProcessorClock; // 96/400 MHz before/after ClockConfig()

extern void DelayRaw( bit32 Count);
extern void DelayMs( bit32 Milliseconds);

// The following are only valid after a call to FastClockConfig():

extern bit32 DelayRaw_15ns ;
extern bit32 DelayRaw_66ns ;
extern bit32 DelayRaw_400ns;
extern bit32 DelayRaw_50us ;
extern bit32 DelayRaw_200us;
extern bit32 DelayRaw_1ms  ;

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

