//-------------------------------------------------------------------------------------------
//  led.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Use to turn the led on or off.

void Led( bit8 On) {
  if (SysType == e_OptiCell) {
    GpioPut( GPIO_PORTD, BOARD_PORTD_LED_GRN, On ? 0 : 1);
} }

// Use to set the USB LED to a specific color.  Tri-color LED's are active low.

void UsbLedColor( bit32 Color) {
  if (SysType == e_NetBridge) {
    GpioPut( GPIO_PORTC, LED_WHITE, 1); // extinguish all 3 dies
    GpioPut( GPIO_PORTC, Color    , 0); // illuminate selected dies
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

