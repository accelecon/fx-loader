//--------------------------------------------------------------------------
//  lcd.h
//--------------------------------------------------------------------------

#define Color_Blk 0
#define Color_Red 1
#define Color_Yel 2
#define Color_Grn 3

typedef enum {
  Key_Up     = 0x01,
  Key_Enter  = 0x02,
  Key_Cancel = 0x04,
  Key_Left   = 0x08,
  Key_Right  = 0x10,
  Key_Down   = 0x20,
  Key_None   = 0x80,
  Key_Error  = 0xff
} e_LcdKey;

extern void LcdInit( void);
extern void LcdPutRowCol( bit8 Row, bit8 Col, ccptr Text);
extern void LcdPutRow   ( bit8 Row          , ccptr Text);
extern void LcdPutRowColDec( bit8 Row, bit8 Col, bit32 Value);
extern void LcdLedSet( bit8 Led, bit8 Color);
extern e_LcdKey LcdKeyGet( void);
extern void LcdTest( void);
extern bool LcdPosCursor( bit8 Row, bit8 Col);
extern bool LcdSetCursor( bool Flag);

extern ccptr LcdVersion;
extern bit32 LcdInitMs ;

extern bit8 LcdColSerial;
extern bit8 LcdRowSerial;

extern bit8 LcdColLast;
extern bit8 LcdRowLast;

//--------------------------------------------------------------------------
//  end
//--------------------------------------------------------------------------
