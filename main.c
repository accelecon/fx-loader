//-------------------------------------------------------------------------------------------
//  main.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Some handy common strings.

static const cptr StrLoader   = "loader"  ;
static const cptr StrUboot    = "u-boot"  ;
static const cptr StrUbEnv    = "ub-env"  ;
static const cptr StrKernel0  = "kernel0" ;
static const cptr StrKernel1  = "kernel1" ;
static const cptr StrFileSys0 = "filesys0";
static const cptr StrFileSys1 = "filesys1";
static const cptr StrConfig   = "config"  ;
static const cptr StrStore    = "store"   ;

// Initialize FLASH devices and print detected configurations.

static void TtyPutConfiguration( bit8 SdramInit) {
  char DateStamp[12]; // Avoid gcc call to memcpy()!
  StrCpy( DateStamp, __DATE__); // 'Nov 08 2000'
  if (DateStamp[4] == ' ') {
    DateStamp[4] = '0';
  }
  TtyPutStr( "\nAccelerated Concepts FX-Series Loader " LOADER_VERSION " - ");
  TtyPutStr( DateStamp);
  TtyPutStr( " " __TIME__);
  if (SysType == e_OptiCell) {
    TtyPutStr( " (CM "); TtyPutStr( CmVersion);
    TtyPutChr( ')');
  }
  TtyPutLn();
  if (SdramInit) {
    SdramConfig();
    SdramTestQuick( 1);
  }
  TtyPutClock();
  TtyPutStr( "\n    "); TtyPutStr( SdramPart); TtyPutDec( SdramSize, 21); TtyPutStr( " bytes");
    TtyPutStr( ", CL"); TtyPutDec( SdramCasLatency, 1);
    TtyPutStr( ", "); TtyPutStr( SdramBurstMode);
    TtyPutStr( "/"); TtyPutStr( SdramBurstType);
    TtyPutStr( ", Burst Length "); TtyPutDec( SdramBurstLength, 1);
    TtyPutLn();
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) { JF_Print(); TtyPutFlashGeometry( JF_BLOCKS_TOTAL, JF_BLOCKSIZE); }
}

static void DisableInterrupts( void) {
  MemPut32( AT91C_AIC_IDCR, 0xffffffff);
}

// For OptiCell only, assure that the lcd display is properly initialized
// before booting linux.  Note that the LcdInit() routine can take up to 
// three seconds to return if this procedure is called immediately after
// system power, as it waits for the lcd module to boot up, so it is best
// to postpone calling this routine as late as possible.

static void InitializeLcd( bool Force) {
  static bool Initialized = false;
  if ((Initialized == false) || Force) {
    Initialized = true;
    if (SysType == e_OptiCell) {
      LcdInit();
      LcdPutRow( 0, "OptiCell-FX");
      LcdPutRow( 1, uBootEnvSerialGet( GetEnvString( "ethaddr")));
      LcdColSerial = LcdColLast;
      LcdRowSerial = LcdRowLast;
   // LcdPutRowColDec( 1, 0, LcdInitMs);
   // LcdPutRowCol( 1, 4, LcdVersion);
      LcdPutRowCol( 2, 4, "LD=");
      LcdPutRowCol( 2, 7, LOADER_VERSION);
      LcdPutRowCol( 3, 4, "CM=");
      LcdPutRowCol( 3, 7, CmVersion+2); // skip F=
} } }

// Allow manual editing of the serial number.  If the serial number is
// changed, update the u-boot and loader environments.

static void SerialEditLcd( void) {
  if (SysType == e_OptiCell) {
    InitializeLcd( false);
    uBootEnvSerialEditLcd();
    InitializeLcd( true);
} }

static void SerialEditTty( void) {
  uBootEnvSerialGet( GetEnvString( "ethaddr"));
  uBootEnvSerialEditTty();
  InitializeLcd( true);
}

bit8 LoaderProtect = 1;

static void LoadAndJumpTo( ccptr partStr, bit32 addr){
  /* TODO: prompt user for size */
  bit32 size = 512 * 1024;
  TtyPutStr( "\nBooting raw code from ");
  TtyPutStr( partStr);
  TtyPutStrLn( "...");
  bit32 FlashBlocksize;
  FlashBlocksize = JF_BLOCKSIZE;
  RestartWatchdog();
  if (JF_Init( FlashBlocksize)) {
    UsbLedColor( COLOR_LOAD);
    RestartWatchdog();
    if (JF_Read( addr, size, (bptr) BOARD_SDRAM_UBOOT) == FLASH_OK) {
      TtyPutStr( "\nBranching to aritrary code at $"); TtyPutHex(BOARD_SDRAM_UBOOT, 8); TtyPutStrLn( "...");
      UsbLedColor( COLOR_UBOOT);
      uImageGoUboot();
    } else {
      TtyPutStrLn( "\nERROR reading flash.");
    }
  } else {
    TtyPutStrLn( "\nERROR initializing flash.");
} }

// Display menu and automatically launch u-boot if not interrupted.

int main( void) {
  bit8 FastTtyFlag = 0, BootFlag = 0, AutoBoot = 1, AutoLcd = 0;
  DisableInterrupts();
  EnableIcache();
  GpioConfig( e_Generic);
  FastClockConfig(); // assure 100/133 MHz master clock
  TtyConfig( FastTtyFlag, 0);
  SlowClockConfig(); // assure 32 KHz clock on external crystal
  if (CmGetVersion()) {
    GpioConfig( e_OptiCell);
    CmBeep( 880, 4);
  } else {
    GpioConfig( e_NetBridge);
  }
  //DelayMs( 100);
  UsbLedColor( COLOR_INIT);
  //DelayMs( 100);
  TtyPutConfiguration( 1);

  // If Loader enviroment is valid and contains a non-blank ethaddr value, see if it matches
  // the one in the u-boot enviroment.  If it does not, or if the u-boot environment is not
  // valid, restore the entire u-boot enviroment from the loader environment.

//  char LoaderEthAddr[20];
//  if (StrLen( StrCpy( LoaderEthAddr, GetLdrString( "ethaddr")))) {
//    if (StrCmp( LoaderEthAddr, GetEnvString( "ethaddr"))) {
//      if (StrLen( GetLdrString( "ethaddr"))) { // get loader environment image back into buffer
//        uBootEnvSave( JF_ADDR_UBENV, UBENV_BYTES);
//  } } }

  // Turn off the watchdog if requested by wdog=0 in the u-boot environment.
  
  if (StrCmp( GetEnvString( "wdog"), "0") == 0) {
    DisableWatchdog(); // saw wdog=0 in u-boot environment
  }

  bit32 LoopCount = 0;
  char Partition = 0, DisplayMenu = 1, ShowAll = 0, Switch = 0;

  // menu keys are:
  //
  //  0-5 . . partition select, then:
  //    * . . . partition erase
  //    c . . . partition check
  //    d . . . partition dump
  //    j . . . jump to arbitrary code after loading into ram
  //    l . . . partition start/select (linux)
  //    t . . . partition test (destructive)
  //    u . . . partition upload (xmodem)
  //
  //  g . . . test gpio (leds, usb power)
  //  i . . . identify flash
  //  m . . . test memory
  //  r . . . reset (reboot)
  //  s . . . start u-boot
  //  v . . . serial edit
  //  w . . . ub-env reset
  //  y . . . lcd test
  //  z . . . cm test
  //  @ . . . toggle loader protect
  //  ! . . . toggle fast/slow baud
  //  ? . . . toggle full/basic menu
  //  ^ . . . toggle master clock
  //  # . . . toggle slow clock

  while (1) { // loop forever until u-boot is started or the board is reset
    if (DisplayMenu) {
      TtyPutLn();
      DisplayMenuItem( '0', Partition, StrLoader  , JF_ADDR_LOADER  , JF_BLOCKS_LOADER  , e_Loader);
      DisplayMenuItem( '1', Partition, StrUboot   , JF_ADDR_UBOOT   , JF_BLOCKS_UBOOT   , e_uBoot );
      DisplayMenuItem( '2', Partition, StrUbEnv   , JF_ADDR_UBENV   , JF_BLOCKS_UBENV   , e_uEnv  );
      DisplayMenuItem( '3', Partition, StrKernel0 , JF_ADDR_KERNEL0 , JF_BLOCKS_KERNEL0 , e_uImage);
      DisplayMenuItem( '4', Partition, StrKernel1 , JF_ADDR_KERNEL1 , JF_BLOCKS_KERNEL1 , e_uImage);
      DisplayMenuItem( '5', Partition, StrFileSys0, JF_ADDR_FILESYS0, JF_BLOCKS_FILESYS0, e_None  );
      DisplayMenuItem( '6', Partition, StrFileSys1, JF_ADDR_FILESYS1, JF_BLOCKS_FILESYS1, e_None  );
      DisplayMenuItem( '7', Partition, StrConfig  , JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  , e_None  );
      DisplayMenuItem( '8', Partition, StrStore   , JF_ADDR_STORE   , JF_BLOCKS_STORE   , e_None  );
      TtyPutStrLn( "\ns: start u-boot");
      if (ShowAll) {
        ShowAll = 0;
        TtyPutStrLn( "m: test memory");
        TtyPutStrLn( "g: test gpio (leds, usb power, delay)");
        TtyPutStrLn( "i: identify flash");
        TtyPutStrLn( "a: serial edit tty");
        TtyPutStrLn( "v: serial edit lcd");
        TtyPutStrLn( "w: ub-env reset");
        TtyPutStrLn( "y: lcd test");
        TtyPutStrLn( "z: cm test");
        TtyPutStrLn( "r: reset (reboot)\n");
        TtyPutStrLn( "@: toggle loader protect");
        TtyPutStrLn( "!: toggle fast/slow baud");
        TtyPutStrLn( "?: toggle full/basic menu");
        TtyPutStrLn( "^: toggle master clock");
        TtyPutStrLn( "#: toggle slow clock");
        TtyPutStrLn( "0-5: select partition, then:");
        TtyPutStrLn( " *: partition erase");
        TtyPutStrLn( " t: partition test (destructive)");
        TtyPutStrLn( " u: partition upload (xmodem)");
        TtyPutStrLn( " d: partition dump");
        TtyPutStrLn( " c: partition check");
        TtyPutStrLn( " l: partition start/select (linux)");
        TtyPutStrLn( " j: partition start (raw)");
      }
      DisplayMenu = 0;
      UsbLedColor( AutoBoot ? COLOR_AUTO : COLOR_MENU);
    }
    bit8 MenuSelection;
    if (TtyGetReady()) {
      MenuSelection = TtyGetChar();
      AutoBoot = 0;
      UsbLedColor( COLOR_MENU);
      DisableWatchdog();
    } else {
      MenuSelection = 0;
      if (GpioGet( GPIO_PORTD, BOARD_PORTD_SW1)) { // switch inactive
        if (Switch == 1) {
          Led( 0);
          UsbLedColor( AutoBoot ? COLOR_AUTO : COLOR_MENU);
          DelayMs( 10);
          Switch = 0;
        }
      } else {
        if (Switch == 0) {
          Led( 1);
          if (SysType == e_OptiCell) {
            AutoLcd = 1;
          }
          UsbLedColor( COLOR_SWITCH);
          DelayMs( 10);
          Switch = 1;
    } } }
    if (AutoBoot) {
      LoopCount++;
      DelayMs( 1);
    }
    switch (MenuSelection) {
      case '0': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_LOADER  ; break;
      case '1': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_UBOOT   ; break;
      case '2': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_UBENV   ; break;
      case '3': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_KERNEL0 ; break;
      case '4': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_KERNEL1 ; break;
      case '5': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_FILESYS0; break;
      case '6': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_FILESYS1; break;
      case '7': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_CONFIG  ; break;
      case '8': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_STORE   ; break;
      case '*':
        switch (Partition) {
          case '0': ZapJedecFlash( StrLoader  , JF_ADDR_LOADER  , JF_BLOCKS_LOADER  ); break;
          case '1': ZapJedecFlash( StrUboot   , JF_ADDR_UBOOT   , JF_BLOCKS_UBOOT   ); break;
          case '2': ZapJedecFlash( StrUbEnv   , JF_ADDR_UBENV   , JF_BLOCKS_UBENV   ); break;
          case '3': ZapJedecFlash( StrKernel0 , JF_ADDR_KERNEL0 , JF_BLOCKS_KERNEL0 ); break;
          case '4': ZapJedecFlash( StrKernel1 , JF_ADDR_KERNEL1 , JF_BLOCKS_KERNEL1 ); break;
          case '5': ZapJedecFlash( StrFileSys0, JF_ADDR_FILESYS0, JF_BLOCKS_FILESYS0); break;
          case '6': ZapJedecFlash( StrFileSys1, JF_ADDR_FILESYS1, JF_BLOCKS_FILESYS1); break;
          case '7': ZapJedecFlash( StrConfig  , JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  ); break;
          case '8': ZapJedecFlash( StrStore   , JF_ADDR_STORE   , JF_BLOCKS_STORE   ); break;
        }
        break; 
      case 't':
        switch (Partition) {
          case '0': TestJedecFlash( StrLoader  , JF_ADDR_LOADER  , JF_BLOCKS_LOADER  ); break;
          case '1': TestJedecFlash( StrUboot   , JF_ADDR_UBOOT   , JF_BLOCKS_UBOOT   ); break;
          case '2': TestJedecFlash( StrUbEnv   , JF_ADDR_UBENV   , JF_BLOCKS_UBENV   ); break;
          case '3': TestJedecFlash( StrKernel0 , JF_ADDR_KERNEL0 , JF_BLOCKS_KERNEL0 ); break;
          case '4': TestJedecFlash( StrKernel1 , JF_ADDR_KERNEL1 , JF_BLOCKS_KERNEL1 ); break;
          case '5': TestJedecFlash( StrFileSys0, JF_ADDR_FILESYS0, JF_BLOCKS_FILESYS0); break;
          case '6': TestJedecFlash( StrFileSys1, JF_ADDR_FILESYS1, JF_BLOCKS_FILESYS1); break;
          case '7': TestJedecFlash( StrConfig  , JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  ); break;
          case '8': TestJedecFlash( StrStore   , JF_ADDR_STORE   , JF_BLOCKS_STORE   ); break;
        }
        break;
      case 'u':
        switch (Partition) {
          case '0': XmodemToJedecFlash( StrLoader  , JF_ADDR_LOADER  , JF_BLOCKS_LOADER  , e_Loader); break;
          case '1': XmodemToJedecFlash( StrUboot   , JF_ADDR_UBOOT   , JF_BLOCKS_UBOOT   , e_uBoot ); break;
          case '2': XmodemToJedecFlash( StrUbEnv   , JF_ADDR_UBENV   , JF_BLOCKS_UBENV   , e_uEnv  ); break;
          case '3': XmodemToJedecFlash( StrKernel0 , JF_ADDR_KERNEL0 , JF_BLOCKS_KERNEL0 , e_uImage); break;
          case '4': XmodemToJedecFlash( StrKernel1 , JF_ADDR_KERNEL1 , JF_BLOCKS_KERNEL1 , e_uImage); break;
          case '5': XmodemToJedecFlash( StrFileSys0, JF_ADDR_FILESYS0, JF_BLOCKS_FILESYS0, e_None  ); break;
          case '6': XmodemToJedecFlash( StrFileSys1, JF_ADDR_FILESYS1, JF_BLOCKS_FILESYS1, e_None  ); break;
          case '7': XmodemToJedecFlash( StrConfig  , JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  , e_None  ); break;
          case '8': XmodemToJedecFlash( StrStore   , JF_ADDR_STORE   , JF_BLOCKS_STORE   , e_None  ); break;
        }
        break;
      case 'd':
        switch (Partition) {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8': DumpJedecFlash(); break;
        }
        break;
      case 'c':
        switch (Partition) {
          case '0': CheckJedecFlash( StrLoader , JF_ADDR_LOADER , JF_BLOCKS_LOADER , e_Loader); break;
          case '1': CheckJedecFlash( StrUboot  , JF_ADDR_UBOOT  , JF_BLOCKS_UBOOT  , e_uBoot ); break;
          case '2': CheckJedecFlash( StrUbEnv  , JF_ADDR_UBENV  , JF_BLOCKS_UBENV  , e_uEnv  ); break;
          case '3': CheckJedecFlash( StrKernel0, JF_ADDR_KERNEL0, JF_BLOCKS_KERNEL0, e_uImage); break;
          case '4': CheckJedecFlash( StrKernel1, JF_ADDR_KERNEL1, JF_BLOCKS_KERNEL1, e_uImage); break;
        }
        break;
      case 'l':
        switch (Partition) {
          case '3': uImageGoLinux( StrKernel0, JF_ADDR_KERNEL0, JF_BLOCKS_KERNEL0); break;
          case '4': uImageGoLinux( StrKernel1, JF_ADDR_KERNEL1, JF_BLOCKS_KERNEL1); break;
          case '5': uBootFileSysSelect( StrFileSys0, 'a', '5', JF_ADDR_BOOT0); Partition = '2'; break;
          case '6': uBootFileSysSelect( StrFileSys1, 'b', '6', JF_ADDR_BOOT1); Partition = '2'; break;
        }
        break;
      case 'j':
        switch (Partition) {
          case '0': LoadAndJumpTo( StrLoader  , JF_ADDR_LOADER  ); break;
          case '1': LoadAndJumpTo( StrUboot   , JF_ADDR_UBOOT   ); break;
          case '2': LoadAndJumpTo( StrUbEnv   , JF_ADDR_UBENV   ); break;
          case '3': LoadAndJumpTo( StrKernel0 , JF_ADDR_KERNEL0 ); break;
          case '4': LoadAndJumpTo( StrKernel1 , JF_ADDR_KERNEL1 ); break;
          case '5': LoadAndJumpTo( StrFileSys0, JF_ADDR_FILESYS0); break;
          case '6': LoadAndJumpTo( StrFileSys1, JF_ADDR_FILESYS1); break;
          case '7': LoadAndJumpTo( StrConfig  , JF_ADDR_CONFIG  ); break;
          case '8': LoadAndJumpTo( StrStore   , JF_ADDR_STORE   ); break;
        }
        break;
      case 'm': SdramTestFull( SdramSize, 1); break;
      case 'i': 
        TtyPutConfiguration( 0);
        break;
      case 'g': GpioTest(); break;
      case 'a': SerialEditTty(); break;
      case 'v': SerialEditLcd(); break;
      case 'w': uBootEnvReset( JF_ADDR_UBENV, JF_BLOCKS_UBENV * JF_BLOCKSIZE, true); break;
      case 'y':
        InitializeLcd( true);
        LcdTest();
        break;
      case 'z': CmTest(); break;
      case 'r': Reset(); break;
      case '?': ShowAll = 1; break;
      case 's': BootFlag = 1; break;
      case '@': LoaderProtect = (LoaderProtect ? 0 : 1); break;
      case '!': FastTtyFlag = TtyConfig( FastTtyFlag ? 0 : 1, 1); MenuSelection = 0; break; // don't redisplay menu
      case '^':
        FastClockConfig();
        TtyConfig( FastTtyFlag, 0);
        TtyPutConfiguration( 0);
        break;
      case '#':
        SlowClockConfig();
        TtyConfig( FastTtyFlag, 0);
        TtyPutConfiguration( 0);
        break;
      case '=': break; // just redisplay menu
      case ' ': AutoBoot = 0; // disable autoboot
      default: MenuSelection = 0; // ignore all other characters and don't redisplay menu
    }
    
    // If BootFlag is set, then we have been explicitly asked to start uBoot.  If LoopCount gets to 10000
    // and AutoBoot is set, then 2 seconds have elapsed since boot with no keystroke activity, so we want
    // to check the uBoot environment for a bootm setting, and if found then boot linux directly.  If not
    // found, then punt and start uBoot.
    
    if (BootFlag || ((LoopCount > 2000) && AutoBoot)) { // 2 second delay
      CmBeep( 660, 4);
      InitializeLcd( false);
      CmBeep( 880, 4);
      if (AutoBoot) {
        ccptr BootPart = GetEnvString( "bootpart");
        switch (BootPart[0]) {
          case 'a': uBootFileSysSelect( StrFileSys0, 'a', '5', JF_ADDR_BOOT0); break;
          case 'b': uBootFileSysSelect( StrFileSys1, 'b', '6', JF_ADDR_BOOT1); break;
      } }
      LoopCount = AutoBoot = 0;
      if (AutoLcd) {
        SerialEditLcd();
      }
      if (BootFlag == 0) {
        ccptr BootCmd = GetEnvString( "bootcmd");
        if (BootCmd && (Compare( (bptr) BootCmd, (bptr) "bootm ", 6, 0) == 0)) {
          bit32 Address = HexValue( BootCmd+6);
          Address -= JF_ADDR_BASE; // translate physical address to virtual address
          // First try the bootm address then fall back to the alternate partition.
          if (Address == JF_ADDR_KERNEL0) uImageGoLinux( StrKernel0, JF_ADDR_KERNEL0, JF_BLOCKS_KERNEL0);
                                          uImageGoLinux( StrKernel1, JF_ADDR_KERNEL1, JF_BLOCKS_KERNEL1);
                                          uImageGoLinux( StrKernel0, JF_ADDR_KERNEL0, JF_BLOCKS_KERNEL0);
      } }
      TtyPutStr( "\nLoading u-boot (");
      TtyPutDec( JF_BLOCKS_UBOOT * JF_BLOCKSIZE, 1);
      TtyPutStrLn(" bytes)...");
      if (JF_Init( JF_BLOCKSIZE)) {
        UsbLedColor( COLOR_LOAD);
        if (JF_Read( JF_ADDR_UBOOT, JF_BLOCKS_UBOOT * JF_BLOCKSIZE, (bptr) BOARD_SDRAM_UBOOT) == FLASH_OK) {
          if (uBootCheckImage( JF_ADDR_UBOOT, JF_BLOCKS_UBOOT * JF_BLOCKSIZE, (bptr) BOARD_SDRAM_UBOOT, /*e_Silent*/ e_Verbose)) {
            TtyPutStr( "Starting u-boot at 0x"); TtyPutHex( BOARD_SDRAM_UBOOT, 8); TtyPutStr("...\n");
            UsbLedColor( COLOR_UBOOT);
            CmBeep( 660, 4);
            uImageGoUboot();
          } else {
            TtyPutStrLn( "*** Image validation failure!");
            GpioTest(); // flash a bunch of lights
            Reset(); // reboot
          }
        } else {
          TtyPutFailure( StrRead);
        }
      } else {
        TtyPutFailure( StrInit);
    } }
    if (MenuSelection && (MenuSelection != '!') && (MenuSelection != ' ')) {
      DisplayMenu = 1;
} } } // no way out of command loop except booting

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

