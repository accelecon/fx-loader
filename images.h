//-------------------------------------------------------------------------------------------
//  images.h
//-------------------------------------------------------------------------------------------

typedef enum { e_Silent, e_Quiet, e_Verbose } e_DisplayLevel;

// uImage header, all data big-endian, followed immediately by data.

typedef struct {
  bit32 HeaderMagic  ; // UIMAGE_MAGIC
  bit32 HeaderCrc    ;
  bit32 CreateTime   ;
  bit32 DataSize     ;
  bit32 DataLoad     ;
  bit32 EntryPoint   ;
  bit32 DataCrc      ;
  bit8  SystemType   ;
  bit8  ArchType     ;
  bit8  ImageType    ;
  bit8  CompType     ;
  char  ImageName[32];
} s_uImageHeader, * p_uImageHeader;

#define uImageHeaderSize sizeof(s_uImageHeader)

// Uboot header, all data big-endian, followed immediately by zImage - 64 bytes

typedef struct {
  bit32 Pad1[5] ; // vectors
  bit32 DataSize; // unused vector
  bit32 Pad2[9] ; // indirect pointers
  bit32 DataCrc ; // initially UBOOT_MAGIC
} s_UbootHeader, * p_UbootHeader;

#define UbootHeaderSize sizeof(s_UbootHeader)

// zImage header - 512 bytes

typedef struct {
  bit32 Pad1[9]  ;
  bit32 Magic    ; // ZIMAGE_MAGIC
  bit32 Start    ;
  bit32 End      ;
  bit32 Pad2[116];
} s_zImageHeader, * p_zImageHeader;

#define zImageHeaderSize sizeof(s_zImageHeader)

// For uImage, we can do a quick check of the header, or a full check of the data.  Because
// the header crc calculation requires modifying the header, we also implement a header
// copy function.

extern p_uImageHeader uImageCopyHeader ( p_uImageHeader Header);
extern bit8           uImageCheckHeader( p_uImageHeader Header, bit32 PartitionBytes, e_DisplayLevel DisplayLevel);
extern bit8           uImageCheckData  ( p_uImageHeader Header, bptr Data);

// For uBoot, we want to do a quick check of a raw, unstamped image, or a full check of a
// stamped image (as just read from flash).  We can also stamp a raw image with a length
// and crc before writing it to flash.

extern bit8 uBootValidHeader( p_UbootHeader Header, bit32 ImageBytes, bit32 PartitionBytes);
extern void uBootStampHeader( p_UbootHeader Header, bit32 ImageBytes);
extern bit8 uBootCheckImage( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, e_DisplayLevel DisplayLevel);

// For uImage, boot and go!

extern void uImageGoLinux( ccptr PartitionName, bit32 FlashStartAddress, bit32 FlashBlocks);

extern bit32 uImageCrc32( bit32 Crc, bptr Buffer, bit32 Length);

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

